/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.personal.listener;

import network.stratus.api.personal.StratusAPIPersonal;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.permissions.PermissionAttachment;

/**
 * Listens on events in order to handle and manage participants in a Ranked
 * match. This includes forcing people onto their registered teams, recording
 * matches starting and ending, preventing team switching and preventing
 * unregistered players joining teams.
 * 
 * @author Ian Ballingall
 *
 */
public class PersonalListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		PermissionAttachment attachment = event.getPlayer().addAttachment(StratusAPIPersonal.get());
		if (event.getPlayer().getUniqueId().equals(StratusAPIPersonal.get().getOwner()) ||
				StratusAPIPersonal.get().getManagers().contains(event.getPlayer().getName())) {
			for (String permission : StratusAPIPersonal.get().getManagerPermissions()) {
				attachment.setPermission(permission, true);
			}

			event.getPlayer().setWhitelisted(true);
		}
		attachment.setPermission("pgm.join", true);
		attachment.setPermission("pgm.join.choose", true);
	}

}
