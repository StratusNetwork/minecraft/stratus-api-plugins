/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.personal.commands;

import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.personal.StratusAPIPersonal;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;

import java.util.UUID;

/**
 * Commands related to Ranked servers.
 * 
 * @author Ian Ballingall
 *
 */
public class PersonalCommands {

	private static int MANAGER_LIMIT = 3000;

	//TODO Manager commands
	@Command(aliases = "promote",
			desc = "Promote someone to a manager",
			perms = "stratusapi.personal.promote")
	public void onPromote(CommandSender sender, TabCompletePlayer target) {
		if (!(sender instanceof Player)) {
			new SingleAudience(sender).sendMessage("command.error.notplayer");
			return;
		}

		Player player = (Player) sender;
		if (!player.getUniqueId().equals(StratusAPIPersonal.get().getOwner())) {
			new SingleAudience(sender).sendMessage("personal.notowner");
			return;
		}

		if (StratusAPIPersonal.get().getManagers().size() >= MANAGER_LIMIT) {
			new SingleAudience(sender).sendMessage("personal.promote.limit");
			return;
		}

		if (!target.getPlayer().isPresent()) {
			new SingleAudience(sender).sendMessage("personal.promote.offline");
			return;
		}

		Player targetPlayer = target.getPlayer().get();

		if (StratusAPIPersonal.get().getManagers().contains(targetPlayer.getName())) {
			new SingleAudience(sender).sendMessage("personal.promote.alreadymanager");
			return;
		}

		StratusAPIPersonal.get().addManager(targetPlayer);
		new SingleAudience(sender).sendMessage("personal.promote.promoted");
	}

	@Command(aliases = "demote",
			desc = "Demote someone from manager",
			perms = "stratusapi.personal.demote")
	public void onDemote(CommandSender sender, String target) {
		if (!(sender instanceof Player)) {
			new SingleAudience(sender).sendMessage("command.error.notplayer");
			return;
		}

		Player player = (Player) sender;
		if (!player.getUniqueId().equals(StratusAPIPersonal.get().getOwner())) {
			new SingleAudience(sender).sendMessage("personal.notowner");
			return;
		}

		if (!StratusAPIPersonal.get().getManagers().contains(target)) {
			new SingleAudience(sender).sendMessage("personal.demote.notfound");
			return;
		}

		StratusAPIPersonal.get().removeManager(target);
		new SingleAudience(sender).sendMessage("personal.demote.demoted");
	}

	@Command(aliases = { "mc" },
			desc = "Send a message to other managers",
			perms = "stratusapi.personal.managerchat")
	public void onManagerChat(CommandSender sender, @Text String message) {
		new MultiAudience.Builder().includePermission("stratusapi.personal.managerchat").build().sendMessage(
				"managerchat.message",
				(sender instanceof Player) ? ((Player) sender).getDisplayName() : Chat.OFFLINE_COLOR + "CONSOLE",
				message);
	}

}
