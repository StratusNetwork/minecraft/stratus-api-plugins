/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.personal;

import network.stratus.api.models.SendChat;
import network.stratus.api.personal.chat.PersonalDisplayNameManager;
import network.stratus.api.personal.listener.PersonalListener;
import network.stratus.api.personal.task.EmptyServerTask;
import network.stratus.api.personal.task.PullMapsTask;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.DisplayNameManager;
import network.stratus.api.bukkit.commands.modules.StratusBukkitCommandModule;
import network.stratus.api.personal.commands.PersonalCommands;
import org.bukkit.scheduler.BukkitRunnable;
import tc.oc.pgm.api.PGM;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Extensions to the Stratus API for Ranked gameplay. This requires PGM
 * extensions. Provides participant management implementations for Ranked.
 * 
 * @author Ian Ballingall
 *
 */
public class StratusAPIPersonal extends JavaPlugin {

	private static StratusAPIPersonal plugin;

	private UUID owner;

	private Set<String> managers;

	private List<String> ownerPermissions;
	private List<String> managerPermissions;

	private int emptyCooldown;

	@Override
	public void onEnable() {
		plugin = this;

		saveDefaultConfig();

		this.emptyCooldown = getConfig().getInt("emptyCooldown", 300);
		new EmptyServerTask()
				.runTaskTimer(StratusAPI.get(), 20, 20);

		if (getConfig().getBoolean("pullmaps", true)) {
			new PullMapsTask()
					.runTaskTimer(StratusAPI.get(), 20, 20);
		}

		// Set up personal prefix manager
		// TODO: remove, as PersonalDisplayNameManager is deprecated
		String ownerPrefix = getConfig().getString("ownerPrefix", "");
		String managerPrefix = getConfig().getString("managerPrefix", "");
		if (!managerPrefix.isEmpty()) {
			// Remove existing manager's events
			DisplayNameManager current = StratusAPI.get().getDisplayNameManager();
			if (current instanceof Listener)
				HandlerList.unregisterAll((Listener) current);

//			PersonalDisplayNameManager displayManager = new PersonalDisplayNameManager(ownerPrefix, managerPrefix);
//			StratusAPI.get().setDisplayNameManager(displayManager);
//			PGM.get().getNameDecorationRegistry().setProvider(displayManager);
//			getServer().getPluginManager().registerEvents(displayManager, this);
		}

		this.managers = new HashSet<>();

		String uuid = getConfig().getString("owner");

		if (uuid != null) {
			this.owner = UUID.fromString(uuid);
		}

		this.managers.addAll(getConfig().getStringList("managers"));

		this.managerPermissions = getConfig().getStringList("managerPermissions");
		this.ownerPermissions = getConfig().getStringList("ownerPermissions");

		getServer().getPluginManager().registerEvents(new PersonalListener(),
				this);

		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph(new StratusBukkitCommandModule());
		DispatcherNode root = cmdGraph.getRootDispatcherNode();
		root.registerCommands(new PersonalCommands());
		new BukkitIntake(this, cmdGraph).register();

		getLogger().info("Stratus API Personal extensions enabled");

		//On server boot
		getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
			public void run(){
				if (StratusAPI.get().getSendChatPublisher() != null) {
					SendChat publishMessage = new SendChat(getOwner(), new ArrayList<>(Arrays.asList("personal.online")));
					new BukkitRunnable() {
						@Override
						public void run() {
							try {
								StratusAPI.get().getSendChatPublisher().publish(publishMessage);
							} catch (IOException e) {
								StratusAPI.get().getLogger().severe("Failed to send chat message: " + e);
								e.printStackTrace();
							}
						}
					}.runTaskAsynchronously(StratusAPI.get());
				}
			}
		});
	}

	@Override
	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API Personal extensions disabled");
	}

	/**
	 * Get the current Stratus API plugin instance.
	 * 
	 * @return The plugin instance object
	 */
	public static StratusAPIPersonal get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

	public UUID getOwner() {
		return owner;
	}

	public Set<String> getManagers() {
		return managers;
	}

	public void addManager(Player manager) {
		this.managers.add(manager.getName());

		PermissionAttachment attachment = manager.addAttachment(StratusAPIPersonal.get());

		for (String permission : StratusAPIPersonal.get().getManagerPermissions()) {
			attachment.setPermission(permission, true);
		}
	}

	public void removeManager(String manager) {
		this.managers.remove(manager);

		Player target = getServer().getPlayer(manager);
		if (target != null) {
			target.removeAttachments(StratusAPIPersonal.get());
		}
	}

	public List<String> getManagerPermissions() {
		return managerPermissions;
	}

	public List<String> getOwnerPermissions() {
		return ownerPermissions;
	}

	public int getEmptyCooldown() {
		return emptyCooldown;
	}
}
