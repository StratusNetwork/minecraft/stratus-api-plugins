/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.primetime;

import java.util.Map;
import org.bukkit.plugin.java.JavaPlugin;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import network.stratus.api.bukkit.commands.modules.StratusBukkitCommandModule;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.commands.ParticipantCommands;
import network.stratus.api.pgm.commands.ReadyCommands;
import network.stratus.api.pgm.match.DefaultReadyManager;
import network.stratus.api.pgm.teams.BasicConfigTeamLoader;
import network.stratus.api.pgm.teams.PreferredColourPartySelectStrategy;
import network.stratus.api.pgm.teams.Team;
import network.stratus.api.primetime.commands.AdminCommands;
import network.stratus.api.primetime.match.PrimeTimeMatchListener;
import network.stratus.api.primetime.match.PrimeTimeParticipantManager;

/**
 * Plugin for Stratus Prime Time events.
 * 
 * @author Ian Ballingall
 *
 */
public class PrimeTime extends JavaPlugin {

	private static PrimeTime plugin;

	@Override
	public void onEnable() {
		plugin = this;

		saveDefaultConfig();

		Map<String, Team> teams = new BasicConfigTeamLoader(getConfig().getConfigurationSection("teams"))
				.loadTeamsAsMap();
		StratusAPIPGM.get().setParticipantManager(
				new PrimeTimeParticipantManager(teams, new PreferredColourPartySelectStrategy()));
		getServer().getPluginManager().registerEvents(new PrimeTimeMatchListener(), this);

		StratusAPIPGM.get().setReadyManager(new DefaultReadyManager());

		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph(new StratusBukkitCommandModule());
		DispatcherNode root = cmdGraph.getRootDispatcherNode();
		root.registerCommands(new ReadyCommands());
		root.registerCommands(new AdminCommands());
		root.registerNode("participant").registerCommands(new ParticipantCommands());
		new BukkitIntake(this, cmdGraph).register();

		getLogger().info("Stratus API Prime Time module enabled");
	}

	@Override
	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API Prime Time module disabled");
	}

	public static PrimeTime get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

}
