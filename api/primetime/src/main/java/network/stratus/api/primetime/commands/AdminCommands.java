/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.primetime.commands;

import org.bukkit.command.CommandSender;

import app.ashcon.intake.Command;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.match.ParticipantManager;
import network.stratus.api.pgm.teams.BasicConfigTeamLoader;
import network.stratus.api.primetime.PrimeTime;
import network.stratus.api.primetime.match.PrimeTimeParticipantManager;

/**
 * Commands pertaining to Prime Time administration.
 * 
 * @author Ian Ballingall
 *
 */
public class AdminCommands {

	@Command(aliases = "reloadteams",
			desc = "Reload teams from configuration",
			perms = "stratusapi.command.primetime.reloadteams")
	public void onTeamReload(CommandSender sender) {
		ParticipantManager pm = StratusAPIPGM.get().getParticipantManager();
		if (!(pm instanceof PrimeTimeParticipantManager)) {
			throw new IllegalStateException("Incompatible ParticipantManager type: " + pm.getClass().getSimpleName());
		}

		PrimeTimeParticipantManager ptpm = (PrimeTimeParticipantManager) pm;
		PrimeTime.get().reloadConfig();
		ptpm.setTeams(new BasicConfigTeamLoader(PrimeTime.get().getConfig().getConfigurationSection("teams")).loadTeamsAsMap());

		new SingleAudience(sender).sendMessage("teams.reload.success");
	}

}
