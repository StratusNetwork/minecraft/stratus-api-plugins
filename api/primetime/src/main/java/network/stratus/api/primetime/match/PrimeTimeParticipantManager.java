/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.primetime.match;

import java.util.Map;
import java.util.UUID;

import network.stratus.api.pgm.match.UuidMappedParticipantManager;
import network.stratus.api.pgm.teams.PartySelectStrategy;
import network.stratus.api.pgm.teams.PreferredColourPartySelectStrategy;
import network.stratus.api.pgm.teams.Team;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;

/**
 * Manages participants in a Prime Time match.
 * 
 * @author Ian Ballingall
 *
 */
public class PrimeTimeParticipantManager extends UuidMappedParticipantManager {

	/** Stores the teams as loaded from storage. */
	private Map<String, Team> teams;

	/** Algorithm for selecting parties for teams. */
	private PartySelectStrategy partySelectStrategy;

	public PrimeTimeParticipantManager(Map<String, Team> teams, PartySelectStrategy partySelectStrategy) {
		super();
		this.teams = teams;
		this.partySelectStrategy = partySelectStrategy;
	}

	/**
	 * Get the set of teams as loaded from storage.
	 * 
	 * @return The teams
	 */
	public Map<String, Team> getTeams() {
		return teams;
	}

	/**
	 * Replace the set of teams.
	 * 
	 * @param teams The new set of teams
	 */
	public void setTeams(Map<String, Team> teams) {
		if (partySelectStrategy instanceof PreferredColourPartySelectStrategy)
			((PreferredColourPartySelectStrategy) partySelectStrategy).resetColours();

		this.teams = teams;
	}

	/**
	 * Get the list of player {@link UUID}s for a given team.
	 * 
	 * @param name The name of the team
	 * @return The list of player UUIDs
	 */
	public Team getTeam(String name) {
		return teams.get(name);
	}

	/**
	 * Register a given team to the given {@link Match}.
	 * 
	 * @param name  The name of the team
	 * @param match The {@link Match} to register them to
	 * @return The {@link Party} they were registered to
	 */
	public Party registerTeam(String name, Match match) {
		if (!teams.containsKey(name)) {
			throw new IllegalArgumentException("Team does not exist: " + name);
		}

		Team team = teams.get(name);
		Party party = partySelectStrategy.select(match, team);
		team.getPlayers().forEach(player -> registerParticipant(match, player.getUuid(), party));

		return party;
	}

	/**
	 * Immediately register all teams to {@link Party}s in the given {@link Match}.
	 * 
	 * @param match The {@link Match} to register teams to
	 */
	public void registerAllTeams(Match match) {
		teams.keySet().forEach(name -> registerTeam(name, match));
	}

	@Override
	public void endMatch(Match match) {
		super.endMatch(match);
		partySelectStrategy.cleanup(match);
	}

}
