/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.messaging;

import org.bukkit.scheduler.BukkitRunnable;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.messaging.MessageProcessor;

/**
 * Processes a message which contains a punishment by enforcing it.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentMessageProcessor implements MessageProcessor<BukkitPunishmentFactory> {

	private String serverName;

	public PunishmentMessageProcessor(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public void process(BukkitPunishmentFactory punishment) {
		// Only broadcast other servers' punishments
		if (!punishment.getServerName().equals(serverName)) {
			// Need to run synchronously
			new BukkitRunnable() {
				@Override
				public void run() {
					punishment.getObject().enforce(true);
				}
			}.runTask(StratusAPI.get());
		}

		StratusAPI.get().getLogger()
				.info(String.format("Received punishment from %s: %s %s %s: %s", punishment.getServerName(),
						punishment.getIssuer().getUsername(), punishment.getType().toString(),
						punishment.getTarget().getUsername(), punishment.getReason()));
	}

}
