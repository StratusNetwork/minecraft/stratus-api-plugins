/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.util;

import java.util.Date;
import java.util.NoSuchElementException;
import java.util.UUID;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import co.aikar.taskchain.TaskChainTasks.Task;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.client.ErrorResponse;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.i18n.Translator;

/**
 * Contains utility and helper functions.
 * 
 * @author Ian Ballingall
 *
 */
public class Util {

	/**
	 * Calculate the difference between two {@link Date}s in days.
	 * 
	 * @param start The start {@link Date}
	 * @param end   The end {@link Date}
	 * @return The difference in days
	 */
	public static int differenceInDays(Date start, Date end) {
		long difference = end.getTime() - start.getTime();
		return (int) difference / (24 * 60 * 60 * 1000);
	}

	/**
	 * Calculate the difference between two {@link Date}s in hours.
	 * 
	 * @param start The start {@link Date}
	 * @param end   The end {@link Date}
	 * @return The difference in days
	 */
	public static int differenceInHours(Date start, Date end) {
		long difference = end.getTime() - start.getTime();
		return (int) difference / (60 * 60 * 1000);
	}

	/**
	 * Process an {@link Exception} thrown during a command-initiated
	 * {@link co.aikar.taskchain.TaskChain}, logging the error and informing the
	 * command sender.
	 * 
	 * @param e      The {@link Exception}
	 * @param task   The {@link Task} throwing the exception
	 * @param sender The command sender
	 */
	public static void handleCommandException(Exception e, Task<?, ?> task, CommandSender sender) {
		StratusAPI.get().getLogger().severe("Handled exception: " + e);

		if (sender != null) {
			SingleAudience audience = new SingleAudience(sender);
			if (e instanceof RequestFailureException) {
				RequestFailureException rfe = (RequestFailureException) e;
				if (e.getCause() instanceof ClientErrorException) {
					ErrorResponse response = rfe.getResponse().get();
					try {
						audience.sendMessage(response.getTranslation());
					} catch (NoSuchElementException missingTranslation) {
						audience.sendMessageRaw(ChatColor.RED + response.getDescription());
						return;
					}
				} else if (e.getCause() instanceof ServerErrorException) {
					ErrorResponse response = rfe.getResponse().get();
					audience.sendMessage("request.error.1");
					try {
						audience.sendMessage("request.error.2", audience.translate(response.getTranslation()));
					} catch (NoSuchElementException missingTranslation) {
						StratusAPI.get().getLogger()
								.warning("Missing translation: " + response.getTranslation().getKey());
						audience.sendMessage("request.error.2", response.getDescription());
					}
				} else if (e.getCause() instanceof Exception) {
					handleCommandException((Exception) e.getCause(), task, sender);
				} else {
					handleCommandException(new RuntimeException(e), task, sender);
				}
			} else {
				audience.sendMessage("command.error.1");
				audience.sendMessage("command.error.2", e);
			}
		}

		e.printStackTrace();
	}

	/**
	 * Sends a clickable team invite message to the target.
	 * 
	 * @param target   The player being invited
	 * @param teamName The team they are being invited to
	 */
	public static void sendTeamInviteMessage(Player target, String teamName) {
		if (target == null)
			return;

		Translator translator = StratusAPI.get().getTranslator();
		IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a("[{\"text\":\""
				+ String.format(translator.getStringOrDefaultLocale(target.getLocale(), "team.invite.recipient"),
						teamName)
				+ " \",\"hoverEvent\":{\"action\":\"show_text\",\"value\":[{\"text\":\"\",\"color\":\"green\"}]}},{\"text\":\"\n["
				+ translator.getStringOrDefaultLocale(target.getLocale(), "team.invite.accept")
				+ "]\",\"color\":\"green\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":[{\"text\":\""
				+ translator.getStringOrDefaultLocale(target.getLocale(), "team.invite.accept.hover")
				+ "\",\"color\":\"green\"}]},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/teams accept "
				+ teamName
				+ "\"}},{\"text\":\" \",\"hoverEvent\":{\"action\":\"show_text\",\"value\":[{\"text\":\"\",\"color\":\"green\"}]}},{\"text\":\"["
				+ translator.getStringOrDefaultLocale(target.getLocale(), "team.invite.deny")
				+ "]\",\"color\":\"red\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":[{\"text\":\""
				+ translator.getStringOrDefaultLocale(target.getLocale(), "team.invite.deny.hover")
				+ "\",\"color\":\"red\"}]},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/teams deny "
				+ teamName + "\"}}]");
		PacketPlayOutChat packet = new PacketPlayOutChat(comp, (byte) 0);
		((CraftPlayer) target).getHandle().playerConnection.sendPacket(packet);
	}

	/**
	 * Sends a clickable team invite message to the target.
	 * 
	 * @param target   The player being invited
	 * @param teamName The team they are being invited to
	 */
	public static void sendTeamInviteMessage(UUID target, String teamName) {
		sendTeamInviteMessage(StratusAPI.get().getServer().getPlayer(target), teamName);
	}

	/**
	 * Translate a String key using the i8n for a certain command sender
	 *
	 * @param sender the sender to translate for
	 * @param key    the key of the translation
	 * @return
	 */
	public static String translate(CommandSender sender, String key) {
		return StratusAPI.get().getTranslator().getStringOrDefaultLocale(sender.getLocale(), key);
	}

}
