/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a request to issue a punishment against a player.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentCreateRequest implements Request<BukkitPunishmentFactory> {

	private User issuer;
	private String target;
	private String reason;
	private Type type;
	private String duration;
	private boolean silent;

	public PunishmentCreateRequest(User issuer, String target, String reason, Type type, boolean silent) {
		this.issuer = issuer;
		this.target = target;
		this.reason = reason;
		this.type = type;
		this.silent = silent;
	}

	public PunishmentCreateRequest(User issuer, String target, String reason, Type type, String duration, boolean silent) {
		if (duration != null && type != Type.BAN && type != Type.MUTE) {
			throw new IllegalArgumentException("Cannot set duration on type: " + type);
		}

		this.issuer = issuer;
		this.target = target;
		this.reason = reason;
		this.type = type;
		this.duration = duration;
		this.silent = silent;
	}

	public User getIssuer() {
		return issuer;
	}

	public String getTarget() {
		return target;
	}

	public String getReason() {
		return reason;
	}

	public Type getType() {
		return type;
	}

	public String getDuration() {
		return duration;
	}

	public boolean isSilent() {
		return silent;
	}

	@Override
	public String getEndpoint() {
		return "/punishments";
	}

	@Override
	public Class<BukkitPunishmentFactory> getResponseType() {
		return BukkitPunishmentFactory.class;
	}

	@Override
	public BukkitPunishmentFactory make(APIClient client) {
		return client.post(this);
	}

}
