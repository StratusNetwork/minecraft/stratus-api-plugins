/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.listeners;

import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MuteManager;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.LoginData;
import network.stratus.api.bukkit.requests.TeamInviteCheckRequest;
import network.stratus.api.bukkit.responses.TeamInviteCheckResponse;
import network.stratus.api.bukkit.tasks.TimedPunishmentTask;
import network.stratus.api.models.Session;
import network.stratus.api.models.punishment.TimedPunishment;
import network.stratus.api.server.SessionManager;

/**
 * Contains any listeners pertaining to users joining the server. These occur
 * after a successful login, when the player is entering the world.
 *
 * @author Ian Ballingall
 */
public class JoinListener implements Listener {

	/**
	 * Called when the user successfully joins the server. Mutes are applied and the
	 * API is notified of the player's successful join.
	 *
	 * @param event The join event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		StratusAPI.get().getDisplayNameManager().setDisplayName(player);

		// Apply the player's mute if it exists
		LoginData loginData = StratusAPI.get().getLoginManager().getLoginData(player.getUniqueId());
		TimedPunishment mute = loginData.getMute();
		if (mute != null) {
			MuteManager muteManager = StratusAPI.get().getMuteManager();
			muteManager.addMute(mute);
			Date now = new Date();
			Date end = new Date(now.getTime() + mute.getTimeRemaining());
			SingleAudience audience = new SingleAudience(event.getPlayer());
			audience.sendMessage("punishment.mute.message.1");
			audience.sendMessage("punishment.mute.message.2", mute.getReason());
			audience.sendMessage("punishment.mute.message.3",
					StratusAPI.get().getTranslator().getTimeDifferenceString(event.getPlayer().getLocale(), end, now));

			// Set up timed punishment task to track gameplay time
			if (muteManager.isOnlineTimeGameplay()) {
				BukkitTask task = new TimedPunishmentTask.Builder(mute).setPeriodToConfigurationValue().build()
						.execute();
				muteManager.addMuteTask(player.getUniqueId(), task);
			}
		}

		final SessionManager sessionManager = StratusAPI.get().getSessionManager();
		final Session session = new Session(player.getUniqueId(), player.getAddress().getAddress().getHostAddress());
		new BukkitRunnable() {
			@Override
			public void run() {
				sessionManager.saveNewSession(session, StratusAPI.get().getApiClient());
			}
		}.runTaskAsynchronously(StratusAPI.get());
	}

	/**
	 * Checks for a player having invites to teams when joining.
	 *
	 * @param event The join event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void checkTeamInvites(PlayerJoinEvent event) {
		TeamInviteCheckRequest request = new TeamInviteCheckRequest(event.getPlayer().getUniqueId());
		StratusAPI.get().newSharedChain("teams").<TeamInviteCheckResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			response.listInvites(event.getPlayer());
		}).execute();
	}

}
