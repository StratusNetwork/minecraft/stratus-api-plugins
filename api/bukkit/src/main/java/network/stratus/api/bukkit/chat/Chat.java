/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import network.stratus.api.models.User;

/**
 * Chat-related constants.
 * 
 * @author Ian Ballingall
 *
 */
public class Chat {

	/** The colour for offline players' names. */
	public static final ChatColor OFFLINE_COLOR = ChatColor.DARK_AQUA;
	/** The default colour for online players' names. */
	public static final ChatColor ONLINE_COLOR_DEFAULT = ChatColor.AQUA;

	/**
	 * Format the given {@link User}'s name into offline format.
	 * 
	 * @param user The {@link User} whose name to format
	 * @return The offline colour-formatted name
	 */
	public static String offlineUsername(User user) {
		return OFFLINE_COLOR + user.getUsername();
	}

	/**
	 * Obtain the display name for a {@link User}. This will attempt to find the
	 * online player and get their display name. If they do not exist, then the
	 * offline name will be returned, as per {@link Chat#offlineUsername(User)}.
	 * 
	 * @param user The {@link User} whose name to format
	 * @return The user's display name
	 */
	public static String getDisplayName(User user) {
		if (user.get_id() != null && Bukkit.getPlayer(user.get_id()) != null) {
			return Bukkit.getPlayer(user.get_id()).getDisplayName();
		} else {
			return offlineUsername(user);
		}
	}

}
