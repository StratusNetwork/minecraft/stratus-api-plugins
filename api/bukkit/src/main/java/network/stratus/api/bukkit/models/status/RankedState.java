package network.stratus.api.bukkit.models.status;

import java.util.List;

public class RankedState {

    // Note this is largely the same as the class
    // network.stratus.api.ranked.messaging.RankedMatch
    // except with a few unneeded fields removed.

    // The ranked match provides a match ID before we can put it
    // in the normal place, so this is called the "matchIDOverride"
    // because it will replace the normal one if they differ.
    private String matchIDOverride;
    private String map;
    private String gamemode;
    private boolean premium;
    private boolean eloEnabled;
    private String seasonPostfix;
    private List<String> captain1;
    private List<String> captain2;
    private List<String> team1;
    private List<String> team2;
    private String queueID;
    private String channel1ID;
    private String channel2ID;
    private List<String> availableMaps;
    private String team1Veto;
    private String team2Veto;
    private String rankedDiscord;
    private String staffAlertsID;
    private String waitingRoomID;

    public RankedState() {}

    public RankedState(
        String matchIDOverride,
        String map,
        String gamemode,
        boolean premium,
        boolean eloEnabled,
        String seasonPostfix,
        List<String> captain1,
        List<String> captain2,
        List<String> team1,
        List<String> team2,
        String queueID,
        String channel1ID,
        String channel2ID,
        List<String> availableMaps,
        String team1Veto,
        String team2Veto,
        String rankedDiscord,
        String staffAlertsID,
        String waitingRoomID
    ) {
        this.matchIDOverride = matchIDOverride;
        this.map = map;
        this.gamemode = gamemode;
        this.premium = premium;
        this.eloEnabled = eloEnabled;
        this.seasonPostfix = seasonPostfix;
        this.captain1 = captain1;
        this.captain2 = captain2;
        this.team1 = team1;
        this.team2 = team2;
        this.queueID = queueID;
        this.channel1ID = channel1ID;
        this.channel2ID = channel2ID;
        this.availableMaps = availableMaps;
        this.team1Veto = team1Veto;
        this.team2Veto = team2Veto;
        this.rankedDiscord = rankedDiscord;
        this.staffAlertsID = staffAlertsID;
        this.waitingRoomID = waitingRoomID;
    }

    public String getMatchIDOverride() {
        return matchIDOverride;
    }

    public String getMap() {
        return map;
    }

    public String getGamemode() {
        return gamemode;
    }

    public boolean isPremium() {
        return premium;
    }

    public boolean isEloEnabled() {
        return eloEnabled;
    }

    public String getSeasonPostfix() {
        return seasonPostfix;
    }

    public List<String> getCaptain1() {
        return captain1;
    }

    public List<String> getCaptain2() {
        return captain2;
    }

    public List<String> getTeam1() {
        return team1;
    }

    public List<String> getTeam2() {
        return team2;
    }

    public String getQueueID() {
        return queueID;
    }

    public String getChannel1ID() {
        return channel1ID;
    }

    public String getChannel2ID() {
        return channel2ID;
    }

    public List<String> getAvailableMaps() {
        return availableMaps;
    }

    public String getTeam1Veto() {
        return team1Veto;
    }

    public String getTeam2Veto() {
        return team2Veto;
    }

    public String getRankedDiscord() {
        return rankedDiscord;
    }

    public String getStaffAlertsID() {
        return staffAlertsID;
    }

    public String getWaitingRoomID() {
        return waitingRoomID;
    }

}
