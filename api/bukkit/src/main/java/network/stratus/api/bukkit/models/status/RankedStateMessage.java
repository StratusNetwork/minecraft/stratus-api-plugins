package network.stratus.api.bukkit.models.status;

public class RankedStateMessage {

    private String serverName;
    // This may be null if there's no in progress ranked match.
    private RankedState state;

    public RankedStateMessage() {}

    public RankedStateMessage(String serverName, RankedState state) {
        this.serverName = serverName;
        this.state = state;
    }

    public String getServerName() {
        return serverName;
    }

    public RankedState getState() {
        return state;
    }

}
