/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import network.stratus.api.chat.Audience;
import network.stratus.api.i18n.Translation;

/**
 * Represents an audience consisting of multiple targets.
 * 
 * @author Ian Ballingall
 *
 */
public class MultiAudience implements BukkitAudience {

	private Collection<? extends BukkitAudience> targets;

	public MultiAudience(Collection<? extends BukkitAudience> targets) {
		this.targets = targets;
	}

	@Override
	public void sendMessage(Translation translation) {
		for (BukkitAudience target : targets) {
			target.sendMessage(translation);
		}
	}

	@Override
	public void sendMessage(String stringKey) {
		for (BukkitAudience target : targets) {
			target.sendMessage(stringKey);
		}
	}

	@Override
	public void sendMessage(String stringKey, Object... args) {
		for (BukkitAudience target : targets) {
			target.sendMessage(stringKey, args);
		}
	}

	@Override
	public void sendMessageRaw(String message) {
		for (BukkitAudience target : targets) {
			target.sendMessageRaw(message);
		}
	}

	@Override
	public void sendTitle(String titleKey, Object[] titleArgs, String subtitleKey, Object[] subtitleArgs, int fadeIn,
			int stay, int fadeOut) {
		for (BukkitAudience target : targets) {
			target.sendTitle(titleKey, titleArgs, subtitleKey, subtitleArgs, fadeIn, stay, fadeOut);
		}
	}

	@Override
	public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		for (BukkitAudience target : targets) {
			target.sendTitle(title, subtitle, fadeIn, stay, fadeOut);
		}
	}

	@Override
	public void sendTitle(String titleKey, Object[] titleArgs, String subtitleKey, Object[] subtitleArgs,
			int forceDuration) {
		for (BukkitAudience target : targets) {
			target.sendTitle(titleKey, titleArgs, subtitleKey, subtitleArgs, forceDuration);
		}
	}

	@Override
	public void sendTitle(String title, String subtitle, int forceDuration) {
		for (BukkitAudience target : targets) {
			target.sendTitle(title, subtitle, forceDuration);
		}
	}

	@Override
	public void playSound(Location location, Sound sound, float volume, float pitch) {
		for (BukkitAudience target : targets) {
			target.playSound(location, sound, volume, pitch);
		}
	}

	@Override
	public void playSound(Sound sound, float volume, float pitch) {
		targets.forEach(target -> target.playSound(sound, volume, pitch));
	}

	/**
	 * Builds a new {@link Audience} of multiple targets, based on the conditions
	 * they satisfy. Targets can be included or excluded by satisfying
	 * {@link Predicate}s. Convenience methods for permission nodes are included.
	 */
	public static class Builder {

		private Set<CommandSender> targets = new HashSet<>();
		private List<Predicate<CommandSender>> includePredicates = new LinkedList<>();
		private List<Predicate<CommandSender>> excludePredicates = new LinkedList<>();

		/**
		 * Add the specified target to the {@link MultiAudience}.
		 */
		public Builder addTarget(CommandSender target) {
			targets.add(target);
			return this;
		}

		/**
		 * Add the specified targets to the {@link MultiAudience}.
		 */
		public Builder addTargets(Collection<? extends CommandSender> targets) {
			this.targets.addAll(targets);
			return this;
		}

		/**
		 * Add all {@link Player}s to the {@link MultiAudience}.
		 */
		public Builder global() {
			targets.addAll(Bukkit.getOnlinePlayers());
			return this;
		}

		/**
		 * Add the console to the {@link MultiAudience}.
		 */
		public Builder console() {
			targets.add(Bukkit.getConsoleSender());
			return this;
		}

		/**
		 * Add all {@link Player}s who have the given permission node.
		 */
		public Builder includePermission(String permission) {
			includePredicates.add(sender -> sender.hasPermission(permission));
			return this;
		}

		/**
		 * Add all {@link Player}s who have the given permission nodes.
		 */
		public Builder includePermissions(Collection<String> permissions) {
			permissions.forEach(this::includePermission);
			return this;
		}

		/**
		 * Exclude {@link Player}s who have the given permission node.
		 */
		public Builder excludePermission(String permission) {
			excludePredicates.add(sender -> sender.hasPermission(permission));
			return this;
		}

		/**
		 * Exclude {@link Player}s who have the given permission nodes.
		 */
		public Builder excludePermissions(Collection<String> permissions) {
			permissions.forEach(this::excludePermission);
			return this;
		}

		/**
		 * Include {@link Player}s who satisfy the given {@link Predicate}.
		 */
		public Builder includePredicate(Predicate<CommandSender> predicate) {
			includePredicates.add(predicate);
			return this;
		}

		/**
		 * Include {@link Player}s who satisfy the given {@link Predicate}s.
		 */
		public Builder includePredicates(Collection<Predicate<CommandSender>> predicates) {
			includePredicates.addAll(predicates);
			return this;
		}

		/**
		 * Exclude {@link Player}s who satisfy the given {@link Predicate}.
		 */
		public Builder excludePredicate(Predicate<CommandSender> predicate) {
			excludePredicates.add(predicate);
			return this;
		}

		/**
		 * Exclude {@link Player}s who satisfy the given {@link Predicate}s.
		 */
		public Builder excludePredicates(Collection<Predicate<CommandSender>> predicates) {
			excludePredicates.addAll(predicates);
			return this;
		}

		/**
		 * Construct the corresponding {@link MultiAudience}. Note that firstly,
		 * {@link Player}s who satisfy the {@code includePredicates} will be added.
		 * Then, of those targets, those satisfying {@code exlucdePredicates} will be
		 * removed.
		 * 
		 * @return The {@link MultiAudience}
		 */
		public MultiAudience build() {
			for (Player player : Bukkit.getOnlinePlayers()) {
				for (Predicate<CommandSender> predicate : includePredicates) {
					if (predicate.test(player)) {
						targets.add(player);
						break;
					}
				}
			}

			targets = targets.stream().filter(target -> {
				for (Predicate<CommandSender> predicate : excludePredicates) {
					if (predicate.test(target)) {
						return false;
					}
				}

				return true;
			}).collect(Collectors.toSet());

			return new MultiAudience(targets.stream().map(SingleAudience::new).collect(Collectors.toSet()));
		}

	}

}
