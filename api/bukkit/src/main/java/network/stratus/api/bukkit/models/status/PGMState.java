package network.stratus.api.bukkit.models.status;

import java.util.List;
import java.util.Map;

public class PGMState {

    private String matchID;
    private String mapName;
    private String phase;
    private List<String> observers;
    private Map<String, String> currentTeam;
    private Map<String, String> primaryTeam;
    private Map<String, Double> scores;
    private Map<String, String> colors;
    private long duration;

    public PGMState() {};

    public PGMState(
        String matchID,
        String mapName,
        String phase,
        List<String> observers,
        Map<String, String> currentTeam,
        Map<String, String> primaryTeam,
        Map<String, Double> scores,
        Map<String, String> colors,
        long duration
    ) {
        this.matchID = matchID;
        this.mapName = mapName;
        this.phase = phase;
        this.observers = observers;
        this.currentTeam = currentTeam;
        this.primaryTeam = primaryTeam;
        this.scores = scores;
        this.colors = colors;
        this.duration = duration;
    }

    public String getMatchID() {
        return matchID;
    }

    public String getMapName() {
        return mapName;
    }

    public String getPhase() {
        return phase;
    }

    public List<String> getObservers() {
        return observers;
    }

    public Map<String, String> getCurrentTeam() {
        return currentTeam;
    }

    public Map<String, String> getPrimaryTeam() {
        return primaryTeam;
    }

    public Map<String, Double> getScores() {
        return scores;
    }

    public Map<String, String> getColors() {
        return colors;
    }

    public long getDuration() {
        return duration;
    }

}
