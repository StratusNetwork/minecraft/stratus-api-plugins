/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.github.paperspigot.Title;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.i18n.Translation;
import network.stratus.api.i18n.Translator;

import java.util.Locale;

/**
 * Represents an audience of a single target.
 * 
 * @author Ian Ballingall
 *
 */
public class SingleAudience implements BukkitAudience {

	private final CommandSender target;

	public SingleAudience(CommandSender target) {
		this.target = target;
	}

	public CommandSender getTarget() {
		return target;
	}

	public Locale getLocale() {
		return target.getLocale();
	}

	@Override
	public void sendMessage(Translation translation) {
		target.sendMessage(translate(translation));
	}

	@Override
	public void sendMessage(String stringKey) {
		target.sendMessage(translate(stringKey));
	}

	@Override
	public void sendMessage(String stringKey, Object... args) {
		target.sendMessage(translate(stringKey, args));
	}

	@Override
	public void sendMessageRaw(String message) {
		target.sendMessage(message);
	}

	@Override
	public void sendTitle(String titleKey, Object[] titleArgs, String subtitleKey, Object[] subtitleArgs, int fadeIn,
			int stay, int fadeOut) {
		if (target instanceof Player) {
			Player player = (Player) target;
			Translator translator = StratusAPI.get().getTranslator();
			String titleText = String.format(translator.getStringOrDefaultLocale(player.getLocale(), titleKey),
					titleArgs);
			String subtitleText = String.format(translator.getStringOrDefaultLocale(player.getLocale(), subtitleKey),
					subtitleArgs);

			player.sendTitle(new Title(titleText, subtitleText, fadeIn, stay, fadeOut));
		}
	}

	@Override
	public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		if (target instanceof Player) {
			((Player) target).sendTitle(new Title(title, subtitle, fadeIn, stay, fadeOut));
		}
	}

	@Override
	public void sendTitle(String titleKey, Object[] titleArgs, String subtitleKey, Object[] subtitleArgs,
			int forceDuration) {
		if (target instanceof Player) {
			Player player = (Player) target;
			Translator translator = StratusAPI.get().getTranslator();
			String titleText = String.format(translator.getStringOrDefaultLocale(player.getLocale(), titleKey),
					titleArgs);
			String subtitleText = String.format(translator.getStringOrDefaultLocale(player.getLocale(), subtitleKey),
					subtitleArgs);

			sendTitle(titleText, subtitleText, forceDuration);
		}
	}

	@Override
	public void sendTitle(String titleText, String subtitleText, int forceDuration) {
		if (target instanceof Player) {
			Player player = (Player) target;
			Title title = new Title(titleText, subtitleText, 0, 20, 5);
			new BukkitRunnable() {
				private final int maximum = (forceDuration * 20) / 5;
				private int counter = 0;

				@Override
				public void run() {
					if (counter++ > maximum) {
						this.cancel();
					}

					player.sendTitle(title);
				}
			}.runTaskTimer(StratusAPI.get(), 0, 5);
		}
	}

	@Override
	public void playSound(Location location, Sound sound, float volume, float pitch) {
		if (target instanceof Player) {
			((Player) target).playSound(location, sound, volume, pitch);
		}
	}

	@Override
	public void playSound(Sound sound, float volume, float pitch) {
		if (target instanceof Player) {
			Player targetPlayer = (Player) target;
			targetPlayer.playSound(targetPlayer.getLocation(), sound, volume, pitch);
		}
	}

	/**
	 * Translates the given {@link Translation} into the target's {@link Locale}.
	 * 
	 * @param translation The {@link Translation} containing translation key and
	 *                    values
	 * @return The translated string
	 */
	public String translate(Translation translation) {
		return translation.translate(StratusAPI.get().getTranslator(), target.getLocale());
	}

	/**
	 * Translates the given key into the target's {@link Locale}.
	 * 
	 * @param stringKey The translation key
	 * @return The translated string
	 */
	public String translate(String stringKey) {
		return StratusAPI.get().getTranslator().getStringOrDefaultLocale(target.getLocale(), stringKey);
	}

	/**
	 * Translates the given key into the target's {@link Locale} and substitutes in
	 * the arguments.
	 * 
	 * @param stringKey The translation key
	 * @param args      The arguments to be substituted
	 * @return The translated string
	 */
	public String translate(String stringKey, Object... args) {
		String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(target.getLocale(), stringKey);
		return String.format(message, args);
	}

	/**
	 * The {@code Audience} consisting solely of the console.
	 */
	public static final SingleAudience CONSOLE = new SingleAudience(StratusAPI.get().getServer().getConsoleSender());

}
