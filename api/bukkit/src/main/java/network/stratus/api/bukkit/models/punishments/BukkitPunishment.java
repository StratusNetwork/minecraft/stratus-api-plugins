/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import javax.annotation.Nullable;

import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.Punishment;

/**
 * Abstract representation of a {@link Punishment}, optionally encapsulating the
 * {@link Player}s who issued and receive the punishment.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class BukkitPunishment extends Punishment {

	protected @Nullable Player issuerPlayer;
	protected @Nullable Player targetPlayer;

	public BukkitPunishment(String _id, User issuer, User target, Date time, Date expiry, String reason, boolean active,
			int number, String serverName, boolean silent) {
		super(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
	}

	@Override
	public Punishment initialisePlayers() {
		this.issuerPlayer = (issuer == null) ? null : StratusAPI.get().getServer().getPlayer(issuer.get_id());
		this.targetPlayer = StratusAPI.get().getServer().getPlayer(target.get_id());
		return this;
	}

}
