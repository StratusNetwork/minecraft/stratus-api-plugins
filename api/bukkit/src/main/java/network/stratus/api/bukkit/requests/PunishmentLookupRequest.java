/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import network.stratus.api.bukkit.responses.PunishmentLookupResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a request to the API to lookup a player's punishments.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentLookupRequest implements Request<PunishmentLookupResponse> {

	/** The player whose punishments will be viewed. */
	private String target;
	/** The types of punishments to list. */
	private List<Type> types;

	public PunishmentLookupRequest(String target, List<Type> types) {
		this.target = target;
		this.types = types;
	}

	public PunishmentLookupRequest(String target, Type... types) {
		this.target = target;
		this.types = Arrays.asList(types);
	}

	public String getTarget() {
		return target;
	}

	public List<Type> getTypes() {
		return types;
	}

	@Override
	public String getEndpoint() {
		return "/punishments/name/" + target.replaceAll("/", "%2F");
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> params = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < types.size() - 1; i++) {
			sb.append(types.get(i)).append(",");
		}
		sb.append(types.get(types.size() - 1));
		params.put("types", sb.toString());
		return params;
	}

	@Override
	public Class<PunishmentLookupResponse> getResponseType() {
		return PunishmentLookupResponse.class;
	}

	@Override
	public PunishmentLookupResponse make(APIClient client) {
		return client.get(this);
	}

}
