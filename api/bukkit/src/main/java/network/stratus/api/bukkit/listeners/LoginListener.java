/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.listeners;

import java.util.Date;
import java.util.SortedSet;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.models.LoginData;
import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.bukkit.requests.LoginRequest;
import network.stratus.api.bukkit.responses.LoginResponse;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.i18n.Translator;
import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.models.punishment.TimedPunishment;
import network.stratus.api.permissions.Group;

/**
 * Contains any listeners pertaining to user logins. These occur when the user
 * attempts to connect, before they have joined the server as a player.
 *
 * @author Ian Ballingall
 *
 */
public class LoginListener implements Listener {

	/**
	 * Called when an user attempts to log into the server. This sends player data
	 * to the API in order to check if the player is banned and to retrieve their
	 * permissions.
	 *
	 * @param event The login event
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPreLogin(AsyncPlayerPreLoginEvent event) {
		LoginRequest requestObject = new LoginRequest(event.getUniqueId(), event.getName(),
				event.getAddress().getHostAddress());

		try {
			LoginResponse response = requestObject.make(StratusAPI.get().getApiClient());
			BukkitPunishmentFactory bpf = response.getPunishment();
			Translator translator = StratusAPI.get().getTranslator();

			if (bpf != null) {
				Punishment punishment = bpf.getObject();
				String message = "";
				if (punishment.getExpiry() == null) {
					message = String.format(translator.getStringOrDefaultLocale(translator.getDefaultLocale(),
							"punishment.ban.permanent.message"), punishment.getReason());
				} else if (punishment.getExpiry().before(new Date())) {
					message = String.format(
						translator.getStringOrDefaultLocale(translator.getDefaultLocale(),
								"punishment.ban.automated.message"),
						punishment.getReason());
				} else {
					message = String.format(
							translator.getStringOrDefaultLocale(translator.getDefaultLocale(),
									"punishment.ban.temporary.message"),
							punishment.getReason(), translator.getTimeDifferenceString(translator.getDefaultLocale(),
									punishment.getExpiry(), new Date()));
				}

				event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, message);
				return;
			}

			if (response.isIpBanned()) {
				String message = translator.getString(translator.getDefaultLocale(), "punishment.ipban.message").get();
				event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, message);
				return;
			}

			SortedSet<Group> groups = StratusAPI.get().getPermissionsManager().getGroupsManager()
					.getGroupsFromList(response.getGroups());
			BukkitPunishmentFactory muteBpf = response.getMute();
			StratusAPI.get().getLoginManager().putLoginData(event.getUniqueId(),
					new LoginData(groups, ((muteBpf == null) ? null: (TimedPunishment) muteBpf.getObject())));
		} catch (RequestFailureException e) {
			// If an error occurs, we have to deny connections to the server
			e.printStackTrace();
//			Translator translator = StratusAPI.get().getTranslator();
//			event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER,
//					translator.getStringOrDefaultLocale(translator.getDefaultLocale(), "login.failure"));
		}
	}

	/**
	 * Called after all other pre-login listeners are done. If the player failed
	 * pre-login checks and their connection was rejected, their login data will be
	 * discarded.
	 *
	 * @param event The event object
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPostPreLogin(AsyncPlayerPreLoginEvent event) {
		if (event.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) {
			StratusAPI.get().getLoginManager().popLoginData(event.getUniqueId());
		}
	}

	/**
	 * Called if the player has successfully passed pre-login checks. Permissions
	 * are assigned at this stage.
	 *
	 * @param event The event object
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		LoginData loginData = StratusAPI.get().getLoginManager().getLoginData(player.getUniqueId());

		if (loginData == null) {
			StratusAPI.get().getLogger().warning("Login data was missing for " + player.getName());
			return;
		}

		for (Group group : loginData.getGroups()) {
			StratusAPI.get().getPermissionsManager().setPermissions(player, group);
		}
	}

	/**
	 * Called after all other login handlers are done. If the player's login was
	 * disallowed, their login data is discarded.
	 *
	 * @param event The event object
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPostLogin(PlayerLoginEvent event) {
		if (event.getResult() != PlayerLoginEvent.Result.ALLOWED) {
			StratusAPI.get().getLoginManager().popLoginData(event.getPlayer().getUniqueId());
			StratusAPI.get().getPermissionsManager().detachPermissions(event.getPlayer());
		}
	}

}
