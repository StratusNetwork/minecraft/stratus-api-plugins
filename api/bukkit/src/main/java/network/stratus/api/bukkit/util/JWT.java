/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.util;

import com.auth0.jwt.algorithms.Algorithm;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * Class used to generate the JWT token for team registration
 *
 * @author Matthew Arnold
 */
public class JWT {

    private final String key;

    /**
     * Creates a new JWT class that is used to generate JWTs for teams
     *
     * @param key the key used to sign the token
     */
    public JWT(String key) {
        this.key = key;
    }

    /**
     * Generates a token for a specified team
     *
     * @param teamName the name of the team to generate the token for
     * @return the generated token
     */
    public String generateToken(String teamName) {
        return com.auth0.jwt.JWT.create()
                .withClaim("team", teamName)
                .withExpiresAt(Date.from(Instant.now().plus(3, ChronoUnit.HOURS)))
                .sign(Algorithm.HMAC256(key));
    }
}
