/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.responses;

import java.util.List;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.Team;

/**
 * Represents a response to a check for team invites.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamInviteCheckResponse {

	private List<Team> invites;

	public List<Team> getInvites() {
		return invites;
	}

	/**
	 * Sends the invites in this response to the player, if they are online.
	 */
	public void listInvites(Player target) {
		for (Team invite : invites) {
			Util.sendTeamInviteMessage(target, invite.getName());
		}
	}

}
