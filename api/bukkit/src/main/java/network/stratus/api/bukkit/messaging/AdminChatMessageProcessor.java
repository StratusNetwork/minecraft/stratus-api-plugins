/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.messaging;

import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.messaging.MessageProcessor;
import network.stratus.api.models.AdminChatMessage;

/**
 * Processes admin chat messages received remotely over a messaging service.
 * 
 * @author Ian Ballingall
 *
 */
public class AdminChatMessageProcessor implements MessageProcessor<AdminChatMessage> {

	private String serverName;

	public AdminChatMessageProcessor(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public void process(AdminChatMessage message) {
		if (!message.getServerName().equals(serverName)) {
			StratusAPI plugin = StratusAPI.get();
			String senderName;
			if (message.getSender() == null) {
				senderName = Chat.OFFLINE_COLOR + "CONSOLE";
			} else {
				Player sender = plugin.getServer().getPlayer(message.getSender());
				if (sender == null) {
					senderName = Chat.OFFLINE_COLOR + message.getSenderName();
				} else {
					senderName = sender.getDisplayName();
				}
			}

			new MultiAudience.Builder().includePermission("stratusapi.adminchat").build()
					.sendMessage("adminchat.message.server", message.getServerName(), senderName, message.getMessage());
			SingleAudience.CONSOLE.sendMessage("adminchat.message.server", message.getServerName(), senderName,
					message.getMessage());
		}
	}

}
