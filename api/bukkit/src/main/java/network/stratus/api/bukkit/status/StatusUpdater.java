package network.stratus.api.bukkit.status;

import java.io.IOException;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.models.status.PGMStateMessage;
import network.stratus.api.bukkit.models.status.RankedStateMessage;

public class StatusUpdater {
    private static final StatusUpdater instance = new StatusUpdater();
    public static StatusUpdater get() { return instance; }

    public void updatePGMState(PGMStateMessage message) {
        try {
            StratusAPI.get().getLogger().info("Updating PGM status");
            StratusAPI.get().getPgmStatusPublisher().publish(message);
        } catch (IOException e) {
            StratusAPI.get().getLogger().severe("Failed to send pgm status update: " + e);
            e.printStackTrace();
        }
    }

    public void updateRankedState(RankedStateMessage message) {
        try {
            StratusAPI.get().getLogger().info("Updating Ranked status");
            StratusAPI.get().getRankedStatusPublisher().publish(message);
        } catch (IOException e) {
            StratusAPI.get().getLogger().severe("Failed to send ranked status update: " + e);
        }
    }
}
