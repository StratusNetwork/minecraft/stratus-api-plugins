/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import org.bukkit.Location;
import org.bukkit.Sound;

import network.stratus.api.chat.Audience;

/**
 * Represents an audience on a Bukkit server.
 * 
 * @author Ian Ballingall
 *
 */
public interface BukkitAudience extends Audience {

	/**
	 * Sends the given localised title to the target.
	 * 
	 * @param titleKey     The key for the title string
	 * @param titleArgs    The values to be substituted into the title
	 * @param subtitleKey  The key for the subtitle string
	 * @param subtitleArgs The values to be substituted into the subtitle
	 * @param fadeIn       The ticks over which to fade in the title
	 * @param stay         The ticks the title should remain for
	 * @param fadeOut      The ticks over which to fade out the title
	 */
	public void sendTitle(String titleKey, Object[] titleArgs, String subtitleKey, Object[] subtitleArgs, int fadeIn,
			int stay, int fadeOut);

	/**
	 * Sends the given title to the target without translation.
	 * 
	 * @param title    The title string
	 * @param subtitle The subtitle string
	 * @param fadeIn   The ticks over which to fade in the title
	 * @param stay     The ticks the title should remain for
	 * @param fadeOut  The ticks over which to fade out the title
	 */
	public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut);

	/**
	 * Sends the given localised title to the target, forcing its appearance for the
	 * given number of seconds. This title will overwrite any other title sent to
	 * the player for this duration.
	 * 
	 * @param titleKey      The key for the title string
	 * @param titleArgs     The values to be substituted into the title
	 * @param subtitleKey   The key for the subtitle string
	 * @param subtitleArgs  The values to be substituted into the subtitle
	 * @param forceDuration The number of seconds to force this title to appear
	 */
	public void sendTitle(String titleKey, Object[] titleArgs, String subtitleKey, Object[] subtitleArgs,
			int forceDuration);

	/**
	 * Sends the given localised title to the target, forcing its appearance for the
	 * given number of seconds. This title will overwrite any other title sent to
	 * the player for this duration.
	 * 
	 * @param title         The title string
	 * @param subtitle      The subtitle string
	 * @param forceDuration The number of seconds to force this title to appear
	 */
	public void sendTitle(String title, String subtitle, int forceDuration);

	/**
	 * Plays the given sound to the target
	 * 
	 * @param location The location at which to play the sound
	 * @param sound    The sound to play
	 * @param volume   The volume of the sound
	 * @param pitch    The pitch of the sound
	 */
	public void playSound(Location location, Sound sound, float volume, float pitch);

	/**
	 * Plays the given sound to the target on its location
	 *
	 * @param sound  The sound to play
	 * @param volume The volume of the sound
	 * @param pitch  The pitch of the sound
	 */
	public void playSound(Sound sound, float volume, float pitch);

}
