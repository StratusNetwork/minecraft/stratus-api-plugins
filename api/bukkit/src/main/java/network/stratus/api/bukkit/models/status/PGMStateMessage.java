package network.stratus.api.bukkit.models.status;

public class PGMStateMessage {

    private String serverName;
    private String issue;
    private PGMState state;

    public PGMStateMessage() {}

    public PGMStateMessage(String serverName, String issue, PGMState state) {
        this.serverName = serverName;
        this.issue = issue;
        this.state = state;
    }

    public String getServerName() {
        return serverName;
    }

    public String getIssue() {
        return issue;
    }

    public PGMState getState() {
        return state;
    }
}
