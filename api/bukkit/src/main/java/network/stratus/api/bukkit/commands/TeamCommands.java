/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.UUID;

import javax.ws.rs.NotFoundException;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.requests.TeamInviteCheckRequest;
import network.stratus.api.bukkit.responses.TeamInviteCheckResponse;
import network.stratus.api.bukkit.server.CooldownManager;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.models.Team;
import network.stratus.api.requests.teams.TeamCreateRequest;
import network.stratus.api.requests.teams.TeamDisbandRequest;
import network.stratus.api.requests.teams.TeamInviteListRequest;
import network.stratus.api.requests.teams.TeamInviteReplyRequest;
import network.stratus.api.requests.teams.TeamInviteRequest;
import network.stratus.api.requests.teams.TeamKickRequest;
import network.stratus.api.requests.teams.TeamLeaderPromoteRequest;
import network.stratus.api.requests.teams.TeamLeaveRequest;
import network.stratus.api.requests.teams.TeamListRequest;
import network.stratus.api.requests.teams.TeamRequest;
import network.stratus.api.responses.ObjectIdResponse;
import network.stratus.api.responses.teams.TeamInviteListResponse;
import network.stratus.api.responses.teams.TeamTargetInfoResponse;
import network.stratus.api.responses.teams.TeamListResponse;

/**
 * Commands for managing teams.
 * 
 * @author Meeples10
 * @author Ian Ballingall
 *
 */
public class TeamCommands {

	// Use cooldown managers to perform confirmation
	private CooldownManager disbandCooldown;
	private CooldownManager promoteCooldown;

	public TeamCommands(long confirmationTime) {
		this.disbandCooldown = new CooldownManager(confirmationTime);
		this.promoteCooldown = new CooldownManager(confirmationTime);
	}

	@Command(aliases = "create",
			desc = "Create a new team",
			usage = "<name>",
			perms = "stratusapi.command.team.create")
	public void create(@Sender CommandSender sender, @Text String teamName) {
		TeamCreateRequest request = new TeamCreateRequest(((Player) sender).getUniqueId(), teamName);
		StratusAPI.get().newSharedChain("teams").<ObjectIdResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.create.success", teamName);
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "disband",
			desc = "Disband your team",
			perms = "stratusapi.command.team.disband")
	public void delete(@Sender CommandSender sender) {
		UUID uuid = ((Player) sender).getUniqueId();
		SingleAudience audience = new SingleAudience(sender);
		// We're using the cooldown the opposite way around, but it does the job
		if (disbandCooldown.executeIfAllowed(uuid)) {
			audience.sendMessage("team.disband.confirm");
			return;
		}

		TeamDisbandRequest request = new TeamDisbandRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<Void>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			audience.sendMessage("team.disband.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "invite",
			desc = "Invite a player to your team",
			usage = "<username>",
			perms = "stratusapi.command.team.invite")
	public void invite(@Sender CommandSender sender, TabCompletePlayer target) {
		TeamInviteRequest request = new TeamInviteRequest(((Player) sender).getUniqueId(), target.getUsername(), true);
		StratusAPI.get().newSharedChain("teams").<TeamTargetInfoResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			Player targetPlayer = StratusAPI.get().getServer().getPlayer(response.getTarget().get_id());

			new SingleAudience(sender).sendMessage("team.invite.success", Chat.getDisplayName(response.getTarget()),
					response.getTeam());
			Util.sendTeamInviteMessage(targetPlayer, response.getTeam());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "uninvite",
			desc = "Cancel an invite to your team",
			usage = "<username>",
			perms = "stratusapi.command.team.invite")
	public void uninvite(@Sender CommandSender sender, TabCompletePlayer target) {
		TeamInviteRequest request = new TeamInviteRequest(((Player) sender).getUniqueId(), target.getUsername(), false);
		StratusAPI.get().newSharedChain("teams").<TeamTargetInfoResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.invite.cancel.success",
					Chat.getDisplayName(response.getTarget()), response.getTeam());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "accept", "join" },
			desc = "Accept an invite to a team",
			usage = "<team name>",
			perms = "stratusapi.command.team.invite")
	public void accept(@Sender CommandSender sender, @Text String teamName) {
		TeamInviteReplyRequest request = new TeamInviteReplyRequest(((Player) sender).getUniqueId(), teamName, true);
		StratusAPI.get().newSharedChain("teams").<Team>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(team -> {
			new SingleAudience(sender).sendMessage("team.accept.success", team.getName());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "deny", "reject" },
			desc = "Reject an invitation to join a team",
			usage = "<team name>",
			perms = "stratusapi.command.team.invite")
	public void reject(@Sender CommandSender sender, @Text String teamName) {
		TeamInviteReplyRequest request = new TeamInviteReplyRequest(((Player) sender).getUniqueId(), teamName, false);
		StratusAPI.get().newSharedChain("teams").<Team>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(team -> {
			new SingleAudience(sender).sendMessage("team.reject.success", team.getName());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "promote", "setleader" },
			desc = "Change the team leader",
			usage = "<username>",
			perms = "stratusapi.command.team.promote")
	public void promote(@Sender CommandSender sender, TabCompletePlayer target) {
		UUID uuid = ((Player) sender).getUniqueId();
		SingleAudience audience = new SingleAudience(sender);
		if (promoteCooldown.executeIfAllowed(uuid)) {
			audience.sendMessage("team.promote.confirm", target.getUsername());
			return;
		}

		TeamLeaderPromoteRequest request = new TeamLeaderPromoteRequest(uuid, target.getUsername());
		StratusAPI.get().newSharedChain("teams").<TeamTargetInfoResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			Player newLeader = StratusAPI.get().getServer().getPlayer(response.getTarget().get_id());
			audience.sendMessage("team.promote.success", Chat.getDisplayName(response.getTarget()));

			if (newLeader != null) {
				new SingleAudience(newLeader).sendMessage("team.promote.recipient", response.getTeam());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "roster",
			desc = "View the roster your team or the team with the given name",
			usage = "[<team name>]",
			perms = "stratusapi.command.team.roster")
	public void roster(@Sender CommandSender sender, @Default("") @Text String teamName) {
		final TeamRequest request;
		if (teamName.isEmpty()) {
			request = TeamRequest.playerUuid(((Player) sender).getUniqueId());
		} else {
			request = TeamRequest.teamName(teamName);
		}

		StratusAPI.get().newSharedChain("teams").<Team>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(team -> {
			SingleAudience audience = new SingleAudience(sender);
			audience.sendMessage("team.roster.title", team.getName());
//			audience.sendMessage("team.roster.leader", Chat.getDisplayName(team.getLeader()));

			team.getPlayers().forEach(user -> {
				if (!user.equals(team.getLeader())) {
//					audience.sendMessageRaw(Chat.getDisplayName(user));
				}
			});
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "list",
			desc = "List all the teams that exist",
			usage = "[<page>]",
			perms = "stratusapi.command.team.list")
	public void list(CommandSender sender, @Default("1") int page) {
		TeamListRequest request = new TeamListRequest(page);
		StratusAPI.get().newSharedChain("teams").<TeamListResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getTeams().isEmpty()) {
				audience.sendMessage("team.list.empty");
			} else {
				audience.sendMessage("team.list.title", page);
				response.getTeams().forEach(team -> {
//					Player leaderPlayer = StratusAPI.get().getServer().getPlayer(team.getLeader().get_id());
//					audience.sendMessage("team.list.entry", team.getName(),
//							(leaderPlayer == null) ? Chat.OFFLINE_COLOR + team.getLeader().getUsername()
//									: leaderPlayer.getDisplayName());
				});
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "leave",
			desc = "Leave your team",
			perms = "stratusapi.command.team.leave")
	public void leave(@Sender CommandSender sender) {
		TeamLeaveRequest request = new TeamLeaveRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<Team>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.leave.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "kick",
			desc = "Remove a player from your team",
			usage = "<username>",
			perms = "stratusapi.command.team.kick")
	public void kick(@Sender CommandSender sender, TabCompletePlayer target) {
		TeamKickRequest request = new TeamKickRequest(((Player) sender).getUniqueId(), target.getUsername());
		StratusAPI.get().newSharedChain("teams").<TeamTargetInfoResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.kick.success", response.getTarget().getUsername());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "invites",
			desc = "View players with pending invites to join your team, or see invitations you've received "
					+ "if you aren't on a team",
			perms = "stratusapi.command.team.invite.list")
	public void listInvites(@Sender CommandSender sender) {
		TeamInviteListRequest request = new TeamInviteListRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<TeamInviteListResponse>asyncFirst(() -> {
			try {
				// Attempt to get invites for the player's team
				return request.make(StratusAPI.get().getApiClient());
			} catch (RequestFailureException e) {
				if (e.getCause() instanceof NotFoundException) {
				// Show the player's received invites if they aren't on a team
					listReceivedInvites((Player) sender);
					return null;
				} else {
					throw e;
				}
			}
		}).abortIfNull().syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getInvites().isEmpty()) {
				audience.sendMessage("team.invite.list.empty", response.getName());
			} else {
				audience.sendMessage("team.invite.list.title", response.getName());
				response.getInvites().forEach(user -> {
					audience.sendMessageRaw(Chat.getDisplayName(user));
				});
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	/**
	 * See the invites to teams this player has received.
	 * 
	 * @param sender The sender's {@link UUID}
	 */
	private void listReceivedInvites(Player sender) {
		TeamInviteCheckRequest request = new TeamInviteCheckRequest(sender.getUniqueId());
		StratusAPI.get().newSharedChain("teams").<TeamInviteCheckResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getInvites().isEmpty()) {
				new SingleAudience(sender).sendMessage("team.invite.recipient.none");
			} else {
				new SingleAudience(sender).sendMessage("team.invite.recipient.title");
				response.listInvites(sender);
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
