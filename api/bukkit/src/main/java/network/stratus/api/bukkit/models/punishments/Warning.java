/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import org.bukkit.Sound;

import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a warning that a player has received.
 * 
 * @author Ian Ballingall
 *
 */
public class Warning extends BukkitPunishment {

	public Warning(String _id, User issuer, User target, Date time, Date expiry, String reason, boolean active,
			int number, String serverName) {
		super(_id, issuer, target, time, expiry, reason, active, number, serverName, false);
	}

	@Override
	public void enforce(boolean showServer) {
		initialisePlayers();
		MultiAudience staff = new MultiAudience.Builder().includePermission("stratusapi.punishments.warn.see.others")
				.build();

		if (showServer) {
			staff.sendMessage("punishment.warn.broadcast.server", serverName,
					Chat.getDisplayName(issuer),
					Chat.getDisplayName(target),
					reason);
		} else {
			staff.sendMessage("punishment.warn.broadcast",
					Chat.getDisplayName(issuer),
					Chat.getDisplayName(target),
					reason);
		}

		if (targetPlayer != null) {
			SingleAudience audience = new SingleAudience(targetPlayer);

			// Chat messages
			audience.sendMessage("punishment.warn.title.chat");
			audience.sendMessage("punishment.warn.message.1", reason);
			audience.sendMessage("punishment.warn.message.2", reason);
			audience.sendMessage("punishment.warn.message.3", reason);
			audience.sendMessage("punishment.warn.title.chat");

			// Title message
			audience.sendTitle("punishment.warn.title", new Object[0], "punishment.warn.subtitle",
					new Object[] { reason }, 5);

			// Sound
			audience.playSound(targetPlayer.getLocation(), Sound.WITHER_DEATH, 10, 1);
		}
	}

	@Override
	public Type getType() {
		return Type.WARN;
	}

}
