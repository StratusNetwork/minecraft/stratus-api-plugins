/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.models.Team;
import network.stratus.api.models.User;
import network.stratus.api.permissions.Group;

/**
 * Represents the data corresponding to a {@link User}.
 * 
 * @author Ian Ballingall
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserData extends User {

	private Location location;
	private PGMStatistics pgmStatistics;
	private List<PreviousUsername> usernameHistory;
	private List<BukkitPunishmentFactory> punishments;
	private Team team;
	private Set<Group> groups;

	public Location getLocation() {
		return location;
	}

	public PGMStatistics getPgmStatistics() {
		return pgmStatistics;
	}

	public List<PreviousUsername> getUsernameHistory() {
		return usernameHistory;
	}

	public List<BukkitPunishmentFactory> getPunishments() {
		return punishments;
	}

	public Team getTeam() {
		return team;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	/**
	 * Represents a player's current or last known location on the network.
	 */
	public static class Location {

		private boolean onlineStatus;
		private String serverName;
		private Date time;

		public boolean getOnlineStatus() {
			return onlineStatus;
		}

		public String getServerName() {
			return serverName;
		}

		public Date getTime() {
			return time;
		}

	}

	/**
	 * Represents a {@link User}'s persistent PGM statistics.
	 */
	@JsonIgnoreProperties(value = { "_id" })
	public static class PGMStatistics {

		private int kills;
		private int deaths;
		private int wools;
		private int cores;
		private int flags;
		private int monuments;
		private int playtime;

		public int getKills() {
			return kills;
		}

		public int getDeaths() {
			return deaths;
		}

		public int getWools() {
			return wools;
		}

		public int getCores() {
			return cores;
		}

		public int getFlags() {
			return flags;
		}

		public int getMonuments() {
			return monuments;
		}

		public int getPlaytime() {
			return playtime;
		}

	}

	/**
	 * Represents a {@link User}'s previous username.
	 */
	@JsonIgnoreProperties(value = { "_id" })
	public static class PreviousUsername {

		private String username;
		private Date changedAt;

		public String getUsername() {
			return username;
		}

		public Date getChangedAt() {
			return changedAt;
		}

	}

}
