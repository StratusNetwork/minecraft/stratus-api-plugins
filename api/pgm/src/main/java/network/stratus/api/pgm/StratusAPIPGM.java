/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.afk.AFKManager;
import network.stratus.api.bukkit.commands.modules.StratusBukkitCommandModule;
import network.stratus.api.pgm.chat.MapAuthorManager;
import network.stratus.api.pgm.chat.PGMDisplayNameManager;
import network.stratus.api.pgm.commands.CustomKitCommands;
import network.stratus.api.pgm.commands.ModerationCommands;
import network.stratus.api.pgm.commands.SetNextCommands;
import network.stratus.api.pgm.commands.StatisticsCommands;
import network.stratus.api.pgm.listeners.DropletsListener;
import network.stratus.api.pgm.listeners.StratusListener;
import network.stratus.api.pgm.customkit.CustomKitTracker;
import network.stratus.api.pgm.listeners.ChunkLoadCrashProtection;
import network.stratus.api.pgm.listeners.FlyGlitchProtection;
import network.stratus.api.pgm.listeners.FreezeListener;
import network.stratus.api.pgm.listeners.StatisticsListener;
import network.stratus.api.pgm.listeners.StatusListener;
import network.stratus.api.pgm.listeners.EventsListener;
import network.stratus.api.pgm.match.NullParticipantManager;
import network.stratus.api.pgm.match.NullReadyManager;
import network.stratus.api.pgm.match.ParticipantManager;
import network.stratus.api.pgm.match.ReadyManager;
import network.stratus.api.pgm.models.QuestManager;
import network.stratus.api.pgm.models.SettingManager;
import network.stratus.api.pgm.moderation.FreezeManager;
import network.stratus.api.pgm.requests.PGMUnificationViewRequest;
import network.stratus.api.pgm.unification.DropletsManager;
import network.stratus.api.pgm.statistics.StatisticsManager;
import network.stratus.api.pgm.status.PGMStatusUpdater;

import javax.annotation.Nullable;

import network.stratus.api.pgm.unification.SetNextRotations;
import network.stratus.api.pgm.unification.TokenManager;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.map.MapOrder;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.rotation.pools.MapPool;
import tc.oc.pgm.rotation.MapPoolManager;

/**
 * Extensions to the Stratus API which interact with PGM. This should be
 * deployed on PGM servers. Provides statistics and participant management
 * systems.
 *
 * @author Ian Ballingall
 *
 */

public class StratusAPIPGM extends JavaPlugin {

	private static StratusAPIPGM plugin;

	private StatisticsManager statsManager;
	private ParticipantManager participantManager;
	private FreezeManager freezeManager;
	private ReadyManager readyManager;
	private DropletsManager dropletsManager;
	private QuestManager questManager;
	private TokenManager tokenManager;
	private SettingManager settingManager;
	private SetNextRotations setNextRotations;

	private boolean dropletsEnabled;
	private boolean snTokensEnabled;

	private CustomKitTracker tracker;

	@Override
	public void onEnable() {
		saveDefaultConfig();
		plugin = this;

		freezeManager = new FreezeManager();

		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph(new StratusBukkitCommandModule());
		DispatcherNode root = cmdGraph.getRootDispatcherNode();

		// If statistics enabled, activate related managers, listeners and commands
		if (getConfig().getBoolean("statistics.enabled", true)) {
			statsManager = new StatisticsManager();
			getServer().getPluginManager().registerEvents(new StatisticsListener(), this);
			getServer().getPluginManager().registerEvents(new EventsListener(), this);
			root.registerCommands(new StatisticsCommands());
		}

		if (getConfig().getBoolean("customkits.enabled", true)) {
			tracker = new CustomKitTracker();
			getServer().getPluginManager().registerEvents(new StratusListener(), this);
			root.registerCommands(new CustomKitCommands());
		}

		// No config option for PGM status updates
		// Why do we even have all these config options?
		getServer().getPluginManager().registerEvents(new StatusListener(), this);

		this.dropletsEnabled = getConfig().getBoolean("droplets.enabled", true);
		this.snTokensEnabled = getConfig().getBoolean("sntokens.enabled", false);

		root.registerCommands(new ModerationCommands(freezeManager));
		root.registerCommands(new SetNextCommands());

		new BukkitIntake(this, cmdGraph).register();

		getServer().getPluginManager().registerEvents(new FlyGlitchProtection(), this);
		getServer().getPluginManager().registerEvents(new ChunkLoadCrashProtection(), this);
		getServer().getPluginManager().registerEvents(new FreezeListener(freezeManager), this);

		PGMDisplayNameManager displayManager = new MapAuthorManager();
		StratusAPI.get().setDisplayNameManager(displayManager);
		PGM.get().getNameDecorationRegistry().setProvider(displayManager);
		if (displayManager instanceof Listener)
			getServer().getPluginManager().registerEvents((Listener) displayManager, this);

//		PlayerOrderFactory playerOrderFactory = new GroupPlayerOrder.Factory();
//		PGM.get().getMatchTabManager().setPlayerOrderFactory(playerOrderFactory);
//		PGM.get().getLegacyMatchTabManager().setPlayerOrderFactory(playerOrderFactory);

		participantManager = new NullParticipantManager();
		readyManager = new NullReadyManager();

//		ProtocolLibrary.getProtocolManager().addPacketListener(new LegacyTabMaxPlayersListener(this));

		settingManager = new SettingManager();

		if (dropletsEnabled) {
			questManager = new QuestManager(statsManager);
			dropletsManager = new DropletsManager();
			getServer().getPluginManager().registerEvents(new DropletsListener(dropletsManager), this);

			getServer().getScheduler().runTask(this, () -> {
				PGMUnificationViewRequest request = new PGMUnificationViewRequest();
				StratusAPI.get().newSharedChain("droplets").asyncFirst(() -> {
					return request.make(StratusAPI.get().getApiClient());
				}).syncLast(response -> {
					System.out.println("Loading droplet config");
					dropletsManager.load(response);

					setNextRotations = response.getTokens();
//					tokenManager = response.getSetNextRotations();
				}).execute();
			});
		}

		if (getConfig().getBoolean("afk.excludeObservers", false)) {
			AFKManager.get().addPlayerFilter((player) -> {
				MatchPlayer matchPlayer = PGM.get().getMatchManager().getPlayer(player);
				if (matchPlayer == null) { return false; }
				if (matchPlayer.isParticipating()) { return true; }
				return false;
			});
		}

		scheduleStatusUpdating();

		getLogger().info("Stratus API PGM extensions enabled");
	}

	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API PGM extensions disabled");
	}

	/**
	 * Get the current plugin instance.
	 *
	 * @return The plugin instance object
	 */
	public static StratusAPIPGM get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

	@Nullable
	public StatisticsManager getStatisticsManager() {
		return statsManager;
	}

	public ParticipantManager getParticipantManager() {
		return participantManager;
	}

	public void setParticipantManager(ParticipantManager manager) {
		participantManager = manager;
	}

	public FreezeManager getFreezeManager() {
		return freezeManager;
	}

	public ReadyManager getReadyManager() {
		return readyManager;
	}

	public void setReadyManager(ReadyManager manager) {
		readyManager = manager;
	}

	public CustomKitTracker getKitTracker() {
		return tracker;
	}

	public boolean isDropletsEnabled() {
		return dropletsEnabled;
	}

	public boolean isSnTokensEnabled() {
		return snTokensEnabled;
	}

	public DropletsManager getDropletsManager() {
		return dropletsManager;
	}

	public SetNextRotations getSetNextRotations() {
		return setNextRotations;
	}

	public QuestManager getQuestManager() {
		return questManager;
	}

	public SettingManager getSettingManager() {
		return settingManager;
	}

	public String getDropletsRotation() {
		String rotation = "micro";

		if (getStatisticsManager() != null &&
				getStatisticsManager().getMatchStatistics() != null) {
			if (getStatisticsManager().getMatchStatistics().getRanked().isEnabled()) {
				return "ranked";
			}
		}

		MapOrder mapOrder = PGM.get().getMapOrder();
		if (mapOrder instanceof MapPoolManager) {
			MapPoolManager mapPoolManager = (MapPoolManager) mapOrder;
			MapPool mapPool = mapPoolManager.getActiveMapPool();
			if (mapPool != null) {
				rotation = mapPool.getName().toLowerCase();
			}
		}

		return rotation;
	}

	public void scheduleStatusUpdating() {
		getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
			PGMStatusUpdater.get().updateIfNeeded();
		}, 100, 20);
	}
}
