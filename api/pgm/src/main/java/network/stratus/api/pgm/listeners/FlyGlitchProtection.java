/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.StratusAPIPGM;
import tc.oc.pgm.spawns.events.ParticipantSpawnEvent;

/**
 * Temporary protection against the fly glitch. Allows flight for half a second
 * after spawning, then turns it off again. This should be removed when a full
 * fix is implemented.
 * 
 * @author Ian Ballingall
 *
 */
public class FlyGlitchProtection implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onSpawn(ParticipantSpawnEvent event) {
		Player player = StratusAPIPGM.get().getServer().getPlayer(event.getPlayer().getId());
		if (player == null)
			return;

		StratusAPI.get().newChain().sync(() -> {
			player.setAllowFlight(true);
		}).delay(10).sync(() -> {
			player.setFlying(false);
			player.setAllowFlight(false);
		}).execute();
	}

}
