/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Represents a custom inventory layout for a player on a specific version of a
 * map. This is represented by the ordering of inventory slots - that is, the
 * originally inventory layout's items are swapped into the custom positions.
 * 
 * @author ShinyDialga
 *
 */
public class Quest {

	private String name;
	private String description;
	private String condition;
	private int count;
	private int totalCount;
	private boolean daily;
	private String expire;

	public Quest() {

	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getCondition() {
		return condition;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isDaily() {
		return daily;
	}

	public String getExpire() {
		return expire;
	}

	public boolean isCompleted() {
		return getCount() >= getTotalCount();
	}

	public boolean isPermanent() {
		return getExpire().equalsIgnoreCase("never");
	}

	public boolean isExpired() {
		if (isPermanent()) {
			return false;
		}

		try {
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
//			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date = sdf.parse(getExpire());
			int sevenHours = (7 * 60 * 60 * 1000);
			date = new Date(date.getTime() + sevenHours);
//			System.out.println(date.getDay() +","+ now.getDay() + "," + now.getTime() + "," + date.getTime());
			boolean isDailyExpired = this.daily &&
					date.getDay() == now.getDay() &&
					date.getMonth() == now.getMonth() &&
					date.getYear() == now.getYear();
			return isDailyExpired || date.before(now);
		} catch (ParseException e) {
			return true;
		}
	}
}
