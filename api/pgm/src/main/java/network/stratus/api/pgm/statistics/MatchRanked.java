package network.stratus.api.pgm.statistics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class MatchRanked {
    private boolean enabled;
    private String rankedDiscord;
    private boolean rankedPremium;
    private boolean rankedEloEnabled;
    private String seasonPostfix;
    private Set<UUID> rankedForfeits;
    private String pickString;
    private String queueChannel;
    private List<UUID> captain1;
    private List<UUID> captain2;
    private List<UUID> team1;
    private List<UUID> team2;
    private List<String> availableMaps;
    private String team1Veto;
    private String team2Veto;

    public MatchRanked() {
        this.enabled = false;
        this.rankedPremium = false;
        this.rankedEloEnabled = true;
        this.seasonPostfix = "";
        this.rankedForfeits = new HashSet<>();
        this.pickString = "";
    }

    public String getRankedDiscord() {
        return rankedDiscord;
    }

    public void setRankedDiscord(String rankedDiscord) {
        this.rankedDiscord = rankedDiscord;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setRankedPremium(boolean rankedPremium) {
        this.rankedPremium = rankedPremium;
    }

    public boolean isRankedPremium() {
        return rankedPremium;
    }

    public void setRankedEloEnabled(boolean rankedEloEnabled) {
        this.rankedEloEnabled = rankedEloEnabled;
    }

    public boolean isRankedEloEnabled() {
        return rankedEloEnabled;
    }

    public void setSeasonPostfix(String seasonPostfix) {
        this.seasonPostfix = seasonPostfix;
    }

    public String getSeasonPostfix() {
        return seasonPostfix;
    }

    public Set<UUID> getRankedForfeits() {
        return rankedForfeits;
    }

    public String getPickString() {
        return pickString;
    }

    public void setPickString(String pickString) {
        this.pickString = pickString;
    }

    public String getQueueChannel() {
        return queueChannel;
    }

    public void setQueueChannel(String queueChannel) {
        this.queueChannel = queueChannel;
    }

    public List<UUID> getCaptain1() {
        return captain1;
    }

    public void setCaptain1(List<UUID> captain1) {
        this.captain1 = captain1;
    }

    public List<UUID> getCaptain2() {
        return captain2;
    }

    public void setCaptain2(List<UUID> captain2) {
        this.captain2 = captain2;
    }

    public List<UUID> getTeam1() {
        return team1;
    }

    public void setTeam1(List<UUID> team1) {
        this.team1 = team1;
    }

    public List<UUID> getTeam2() {
        return team2;
    }

    public void setTeam2(List<UUID> team2) {
        this.team2 = team2;
    }

    public List<String> getAvailableMaps() {
        return availableMaps;
    }

    public void setAvailableMaps(List<String> availableMaps) {
        this.availableMaps = availableMaps;
    }

    public String getTeam1Veto() {
        return team1Veto;
    }

    public void setTeam1Veto(String team1Veto) {
        this.team1Veto = team1Veto;
    }

    public String getTeam2Veto() {
        return team2Veto;
    }

    public void setTeam2Veto(String team2Veto) {
        this.team2Veto = team2Veto;
    }
}
