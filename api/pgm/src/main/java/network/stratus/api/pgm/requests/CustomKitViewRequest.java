/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.pgm.models.CustomKit;
import java.util.UUID;

import javax.annotation.Nullable;

/**
 * Represents a request to the API for fetching a player's custom kit on a given
 * map.
 * 
 * @author ShinyDialga
 * @author Ian Ballingall
 *
 */
public class CustomKitViewRequest implements Request<CustomKit> {

	private UUID uuid;
	private String mapId;
	private @Nullable String mapVersion;

	public CustomKitViewRequest(UUID uuid, String mapId) {
		this.uuid = uuid;
		this.mapId = mapId;
	}

	public CustomKitViewRequest(UUID uuid, String mapId, String mapVersion) {
		this.uuid = uuid;
		this.mapId = mapId;
		this.mapVersion = mapVersion;
	}

	public UUID getUuid() {
		return uuid;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/customkit/" + uuid + "/" + mapId + ((mapVersion == null) ? "" : ("/" + mapVersion));
	}

	@Override
	public Class<CustomKit> getResponseType() {
		return CustomKit.class;
	}

	@Override
	public CustomKit make(APIClient client) {
		return client.get(this);
	}

}
