package network.stratus.api.pgm.unification;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SetNextRotations {

    private Map<String, Map<String, Set<String>>> rotations;

    public SetNextRotations() {
        this.rotations = new HashMap<>();
    }

    public Map<String, Map<String, Set<String>>> getRotations() {
        return rotations;
    }
}
