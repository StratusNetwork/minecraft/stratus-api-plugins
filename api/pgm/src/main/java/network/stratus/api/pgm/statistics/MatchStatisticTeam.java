package network.stratus.api.pgm.statistics;

public class MatchStatisticTeam {

    private String name;
    private String color;
    private boolean winner;
    private double score;

    public MatchStatisticTeam(String name, String color, boolean winner, double score) {
        this.name = name;
        this.color = color;
        this.winner = winner;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public boolean isWinner() {
        return winner;
    }

    public double getScore() {
        return score;
    }
}
