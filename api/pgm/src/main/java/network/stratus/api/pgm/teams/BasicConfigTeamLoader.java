/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.teams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.configuration.ConfigurationSection;

/**
 * Loads {@link Team}s from a Bukkit configuration file. This is a basic loader,
 * which bears no consideration for veto ability or unique identifiers.
 * 
 * @author Ian Ballingall
 *
 */
public class BasicConfigTeamLoader implements TeamLoader {

	private ConfigurationSection config;

	public BasicConfigTeamLoader(ConfigurationSection config) {
		this.config = config;
	}

	@Override
	public List<Team> loadTeams() {
		List<Team> teams = new ArrayList<>();
		for (Map.Entry<String, Object> e : config.getValues(false).entrySet()) {
			if (e.getValue() instanceof List<?>) {
				Set<Team.Player> players = ((List<?>) e.getValue()).stream().filter(value -> value instanceof String)
						.map(uuid -> new Team.Player(UUID.fromString((String) uuid), true)).collect(Collectors.toSet());
				teams.add(new Team(null, e.getKey(), players));
			}
		}

		return teams;
	}

	@Override
	public Team loadTeam(String name) {
		List<?> team = config.getList(name);
		Set<Team.Player> players = team.stream().filter(value -> value instanceof String)
				.map(uuid -> new Team.Player(UUID.fromString((String) uuid), true)).collect(Collectors.toSet());
		return new Team(null, name, players);
	}

	/**
	 * Loads the {@link Team}s as a {@link Map} of team name to team object.
	 * 
	 * @return A {@link Map} of team names to their objects
	 */
	public Map<String, Team> loadTeamsAsMap() {
		return loadTeams().stream().collect(Collectors.toMap(Team::getName, Function.identity()));
	}

}
