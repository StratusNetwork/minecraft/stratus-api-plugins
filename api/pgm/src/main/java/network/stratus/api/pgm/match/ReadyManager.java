/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.match;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;

/**
 * Manages the ready status for teams in a {@link Match}.
 * 
 * @author Ian Ballingall
 *
 */
public interface ReadyManager {

	/**
	 * Inform the manager that a new {@link Match} has loaded.
	 *
	 * @param match The {@link Match} which has loaded
	 */
	void newMatch(Match match);

	/**
	 * Inform the manager that the {@link Match} has ended.
	 * 
	 * @param match The {@link Match} which has ended
	 */
	void endMatch(Match match);

	/**
	 * Get a team's ready status.
	 * 
	 * @param party The {@link Party} to check
	 * @return The {@link Party}'s ready status
	 */
	boolean getReadyStatus(Party party);

	/**
	 * Set a team's ready status.
	 * 
	 * @param party  The {@link Party} whose status is changing
	 * @param status The new ready status
	 * @return Whether the status was changed
	 */
	boolean setReadyStatus(Party party, boolean status);

	/**
	 * Toggle a team's ready status.
	 * 
	 * @param party The {@link Party} whose status is changing
	 * @return The new ready status
	 */
	boolean toggleReadyStatus(Party party);

	/**
	 * Determine if all teams in a {@link Match} are ready.
	 * 
	 * @param match The {@link Match}
	 * @return Whether all teams are ready
	 */
	boolean areAllTeamsReady(Match match);

}