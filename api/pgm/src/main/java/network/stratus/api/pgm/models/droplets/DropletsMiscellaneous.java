/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.models.droplets;

import java.util.Map;

/**
 * Represents a custom inventory layout for a player on a specific version of a
 * map. This is represented by the ordering of inventory slots - that is, the
 * originally inventory layout's items are swapped into the custom positions.
 * 
 * @author ShinyDialga
 *
 */
public class DropletsMiscellaneous {

	private Map<String, Double> throwAwayWool;
	private Map<String, Double> killWoolCarrier;
	private Map<String, Double> scorebox;
	private Map<String, Double> topKiller;

	public DropletsMiscellaneous() {

	}

	public Map<String, Double> getThrowAwayWool() {
		return throwAwayWool;
	}

	public Map<String, Double> getKillWoolCarrier() {
		return killWoolCarrier;
	}

	public Map<String, Double> getScorebox() {
		return scorebox;
	}

	public Map<String, Double> getTopKiller() {
		return topKiller;
	}
}
