package network.stratus.api.pgm.unification;

import com.google.common.util.concurrent.AtomicDouble;
import network.stratus.api.pgm.StratusAPIPGM;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MatchDroplets {
    private boolean dropletsEnabled;
    private String dropletsRotation;
    private Map<UUID, AtomicDouble> touchDroplets;
    private Map<UUID, AtomicDouble> killsDroplets;
    private Map<UUID, AtomicDouble> coreDroplets;
    private Map<UUID, AtomicDouble> monumentDroplets;
    private Map<UUID, AtomicDouble> woolDroplets;
    private Map<UUID, AtomicDouble> flagDroplets;
    private Map<UUID, AtomicDouble> scoreboxDroplets;
    private Map<UUID, AtomicDouble> woolCarrierKillDroplets;
    private Map<UUID, AtomicDouble> woolDestroyDroplets;


    public MatchDroplets() {
        this.dropletsEnabled = StratusAPIPGM.get().isDropletsEnabled();
        this.dropletsRotation = StratusAPIPGM.get().getDropletsRotation();
        this.killsDroplets = new HashMap<>();
        this.touchDroplets = new HashMap<>();
        this.coreDroplets = new HashMap<>();
        this.monumentDroplets = new HashMap<>();
        this.woolDroplets = new HashMap<>();
        this.flagDroplets = new HashMap<>();
        this.scoreboxDroplets = new HashMap<>();
        this.woolCarrierKillDroplets = new HashMap<>();
        this.woolDestroyDroplets = new HashMap<>();
    }

    public boolean isDropletsEnabled() {
        return dropletsEnabled;
    }

    public String getDropletsRotation() {
        return dropletsRotation;
    }

    public Map<UUID, AtomicDouble> getTouchDroplets() {
        return touchDroplets;
    }

    public Map<UUID, AtomicDouble> getKillsDroplets() {
        return killsDroplets;
    }

    public Map<UUID, AtomicDouble> getCoreDroplets() {
        return coreDroplets;
    }

    public Map<UUID, AtomicDouble> getMonumentDroplets() {
        return monumentDroplets;
    }

    public Map<UUID, AtomicDouble> getWoolDroplets() {
        return woolDroplets;
    }

    public Map<UUID, AtomicDouble> getFlagDroplets() {
        return flagDroplets;
    }

    public Map<UUID, AtomicDouble> getScoreboxDroplets() {
        return scoreboxDroplets;
    }

    public Map<UUID, AtomicDouble> getWoolCarrierKillDroplets() {
        return woolCarrierKillDroplets;
    }

    public Map<UUID, AtomicDouble> getWoolDestroyDroplets() {
        return woolDestroyDroplets;
    }
}
