package network.stratus.api.pgm.models;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.LoginData;
import network.stratus.api.permissions.Group;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.statistics.StatisticsManager;
import network.stratus.api.pgm.unification.Droplets;
import network.stratus.api.pgm.unification.QuestCondition;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tc.oc.pgm.util.nms.NMSHacks;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class QuestManager {

    private Map<UUID, List<Quest>> playerQuests;

    private StatisticsManager statisticsManager;

    public QuestManager(StatisticsManager statisticsManager) {
        playerQuests = new HashMap<>();

        this.statisticsManager = statisticsManager;
    }

    public Map<UUID, List<Quest>> getPlayerQuests() {
        return playerQuests;
    }

    public void addProgress(UUID uuid, String condition) {
        this.addProgress(uuid, condition, 1);
    }

    public void addProgress(UUID uuid, QuestCondition condition) {
        this.addProgress(uuid, condition.toString(), 1);
    }

    public void addProgress(UUID uuid, QuestCondition condition, int count) {
        this.addProgress(uuid, condition.toString(), count);
    }

    public void addProgress(UUID uuid, String condition, int count) {
        statisticsManager.registerQuestProgress(uuid, condition, count);

        Player player = Bukkit.getPlayer(uuid);
        List<Quest> playerQuests = StratusAPIPGM.get().getQuestManager().getPlayerQuests().get(uuid);
        if (playerQuests != null) {
            List<Quest> completed = new ArrayList<>();
            for (Quest quest : playerQuests) {
                if (quest.isCompleted() || quest.isExpired()) {
                    continue;
                }

                if (quest.getCondition().equalsIgnoreCase(condition)) {
                    quest.setCount(quest.getCount() + count);
                    if (quest.isCompleted() || quest.isExpired()) {
                        completed.add(quest);
                        if (player != null && player.isOnline()) {
                            player.sendMessage(ChatColor.GRAY + "[" +
                                    ChatColor.GOLD.toString() + ChatColor.BOLD + "Quest" +
                                    ChatColor.RESET + ChatColor.GRAY + "] " +
                                    ChatColor.RESET + quest.getName() + ChatColor.GREEN + ChatColor.BOLD + " Completed");
                            player.sendMessage(ChatColor.GRAY.toString() + ChatColor.ITALIC + quest.getDescription());
                        }
                    } else if (player != null && player.isOnline()) {
                        player.sendMessage(ChatColor.GRAY + "[" +
                                ChatColor.GOLD.toString() + ChatColor.BOLD + "Quest" +
                                ChatColor.RESET + ChatColor.GRAY + "] " +
                                ChatColor.RESET + quest.getName() +
                                ChatColor.GRAY + " (" +
                                ChatColor.AQUA + quest.getCount() +
                                ChatColor.GRAY + "/" +
                                ChatColor.AQUA + quest.getTotalCount() +
                                ChatColor.GRAY + ")");
                        player.sendMessage(ChatColor.GRAY.toString() + ChatColor.ITALIC + quest.getDescription());
                    }
                }
            }
            playerQuests.removeAll(completed);
            StratusAPIPGM.get().getQuestManager().getPlayerQuests().put(uuid, playerQuests);
        }
    }
}
