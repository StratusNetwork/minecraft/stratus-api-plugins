package network.stratus.api.pgm.listeners;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.ResourceBundle.Control;
import java.util.stream.Collectors;

import network.stratus.api.pgm.unification.QuestCondition;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import network.stratus.api.pgm.statistics.MatchStatisticTeam;
import network.stratus.api.pgm.statistics.MatchStatistics;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.models.QuestManager;
import network.stratus.api.pgm.statistics.StatisticsManager;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchFinishEvent;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.match.event.MatchStartEvent;
import tc.oc.pgm.api.party.Competitor;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.ParticipantState;
import tc.oc.pgm.api.player.event.MatchPlayerDeathEvent;
import tc.oc.pgm.api.setting.SettingKey;
import tc.oc.pgm.api.setting.SettingValue;
import tc.oc.pgm.api.tracker.info.MeleeInfo;
import tc.oc.pgm.api.tracker.info.TrackerInfo;
import tc.oc.pgm.controlpoint.ControlPoint;
import tc.oc.pgm.core.Core;
import tc.oc.pgm.core.CoreLeakEvent;
import tc.oc.pgm.destroyable.Destroyable;
import tc.oc.pgm.destroyable.DestroyableContribution;
import tc.oc.pgm.destroyable.DestroyableDestroyedEvent;
import tc.oc.pgm.events.PlayerPartyChangeEvent;
import tc.oc.pgm.flag.Flag;
import tc.oc.pgm.flag.event.FlagCaptureEvent;
import tc.oc.pgm.goals.Goal;
import tc.oc.pgm.goals.events.GoalCompleteEvent;
import tc.oc.pgm.goals.events.GoalTouchEvent;
import tc.oc.pgm.score.ScoreMatchModule;
import tc.oc.pgm.tracker.TrackerMatchModule;
import tc.oc.pgm.tracker.info.ProjectileInfo;
import tc.oc.pgm.stats.StatsMatchModule;
import tc.oc.pgm.teams.Team;
import tc.oc.pgm.wool.MonumentWool;
import tc.oc.pgm.wool.PlayerWoolPlaceEvent;

public class EventsListener implements Listener {

    private static final String DEATH = "d";
    private static final String CHANGE_TEAM = "j";
    private static final String TOUCH = "t";
    private static final String HILL = "h";
    private static final String CORE = "c";
    private static final String WOOL = "w";
    private static final String FLAG = "f";
    private static final String MONUMENT = "m";
    private static final String GAPPLE = "g";

    private void recordEvent(String type, String time, String event) {
        StratusAPIPGM.get().getStatisticsManager().registerEvent(time + "," + type + "," + event);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(MatchPlayerDeathEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        ParticipantState killer = event.getKiller();
        MatchPlayer killed = event.getVictim();
        String location = "" + killed.getLocation().getBlockX()
                + "," + killed.getLocation().getBlockY()
                + "," + killed.getLocation().getBlockZ();
        this.recordEvent(EventsListener.DEATH, time, killed.getId()
                + "," + (killer != null ? killer.getId() : null)
                + "," + location
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChangeTeam(PlayerPartyChangeEvent event) {
        if (!StratusAPIPGM.get().getStatisticsManager().isMatchActive()) {
            // If the match hasn't started, or already ended, team changes don't matter.
            return;
        }
        String time = "" + event.getMatch().getDuration().toMillis();
        String joiner = "" + event.getPlayer().getId();
        Party oldTeam = event.getOldParty();
        Party newTeam = event.getNewParty();
        this.recordEvent(EventsListener.CHANGE_TEAM, time, joiner
                + "," + (oldTeam != null ? oldTeam.getNameLegacy() : null)
                + "," + (newTeam != null ? newTeam.getNameLegacy() : null)
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onGoalTouched(GoalTouchEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        ParticipantState toucher = event.getPlayer();
        Goal<?> objective = event.getGoal();
        String objectiveType =
                event.getGoal() instanceof Flag ? "flag"
                : event.getGoal() instanceof MonumentWool ? "wool"
                : event.getGoal() instanceof Core ? "core"
                : event.getGoal() instanceof Destroyable ? "monu"
                : "????";

        this.recordEvent(EventsListener.TOUCH, time, toucher.getId() + "," + objectiveType + "," + objective.getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void hillCaptureEvent(GoalCompleteEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        Competitor team = event.getCompetitor();
        Goal<?> objective = event.getGoal();
        if (!(event.getGoal() instanceof ControlPoint)) { return; }

        this.recordEvent(EventsListener.HILL, time, (team != null ? team.getId() : null)
                + "," + objective.getName()
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onCoreLeak(CoreLeakEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        Core objective = event.getCore();
        Team team = objective.getOwner();

        this.recordEvent(EventsListener.CORE, time, (team != null ? team.getId() : null)
                + "," + objective.getName()
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onMonumentBreak(DestroyableDestroyedEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        Destroyable objective = event.getDestroyable();
        Team team = objective.getOwner();

        this.recordEvent(EventsListener.MONUMENT, time, (team != null ? team.getId() : null)
                + "," + objective.getName()
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onWoolCapture(PlayerWoolPlaceEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        ParticipantState capper = event.getPlayer();
        MonumentWool objective = event.getWool();
        Team team = objective.getOwner();

        this.recordEvent(EventsListener.WOOL, time, (team != null ? team.getId() : null)
                + "," + objective.getName()
                + "," + capper.getId()
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onFlagCapture(FlagCaptureEvent event) {
        String time = "" + event.getMatch().getDuration().toMillis();
        MatchPlayer capper = event.getCarrier();
        Flag objective = event.getGoal();
        Team team = objective.getOwner();

        this.recordEvent(EventsListener.FLAG, time, (team != null ? team.getId() : null)
                + "," + objective.getName()
                + "," + capper.getId()
        );
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemConsume(PlayerItemConsumeEvent event) {
        MatchPlayer player = PGM.get().getMatchManager().getPlayer(event.getPlayer());
        Match match = player.getMatch();
        String time = "" + match.getDuration().toMillis();

        ItemStack item = event.getItem();
        if (item != null
                && item.getType() != null
                && item.getType().equals(Material.GOLDEN_APPLE)) {
            this.recordEvent(EventsListener.GAPPLE, time, "" + player.getId());
        }
    }
}
