/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.pgm.unification.Droplets;

/**
 * Represents a request to the API for fetching unification data
 * 
 * @author ShinyDialga
 * @author Ian Ballingall
 *
 */
public class PGMUnificationViewRequest implements Request<Droplets> {

	public PGMUnificationViewRequest() {

	}

	@Override
	public String getEndpoint() {
		return "/pgm/unification";
	}

	@Override
	public Class<Droplets> getResponseType() {
		return Droplets.class;
	}

	@Override
	public Droplets make(APIClient client) {
		return client.get(this);
	}

}
