package network.stratus.api.pgm.models;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SettingManager {

    private Map<UUID, Map<String, String>> settings;

    public SettingManager() {
        settings = new HashMap<>();
    }

    public Map<UUID, Map<String, String>> getSettings() {
        return settings;
    }

    public String getSetting(UUID uuid, String key) {
        Map<String, String> playerSettings = settings.get(uuid);
        if (playerSettings == null) {
            return "null";
        }

        return playerSettings.getOrDefault(key, "null");
    }
}
