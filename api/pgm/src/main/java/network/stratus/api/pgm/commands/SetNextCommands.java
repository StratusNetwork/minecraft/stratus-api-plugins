/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.requests.SetnextRequest;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.map.MapInfo;
import tc.oc.pgm.api.map.MapOrder;
import tc.oc.pgm.restart.RestartManager;
import tc.oc.pgm.rotation.pools.MapPool;
import tc.oc.pgm.rotation.MapPoolManager;
import tc.oc.pgm.rotation.pools.Rotation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Commands for setnext tokens
 *
 * @author ShinyDialga
 *
 */
public class SetNextCommands {

	@Command(aliases = { "sntoken" },
			desc = "Set the nextmap using a token")
	public void setNext(@Sender CommandSender sender, @Text String map) {
		if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to run this command");
            return;
        }

        if (!StratusAPIPGM.get().isSnTokensEnabled()) {
			sender.sendMessage(ChatColor.RED + "Setnext Tokens are disabled on this server");
			return;
		}

        Player player = (Player) sender;

		if (RestartManager.isQueued()) {
			sender.sendMessage(ChatColor.RED + "Server is restarting. Try again later.");
			return;
		}

		final PGM pgm = PGM.get();

		MapInfo mapInfo = pgm.getMapLibrary().getMap(map);
		if (mapInfo == null) {
			sender.sendMessage(ChatColor.RED + "Map not found");
			return;
		}

		MapOrder order = PGM.get().getMapOrder();
		if (order == null || !(order instanceof MapPoolManager)) {
			sender.sendMessage(ChatColor.RED + "SetNext Tokens cannot be used on this server");
			return;
		}

		MapPoolManager mapPoolManager = (MapPoolManager) order;
		MapPool mapPool = mapPoolManager.getActiveMapPool();

		if (mapPool == null) {
			sender.sendMessage(ChatColor.RED + "SetNext Tokens cannot be used at this time");
		    return;
		}

		int nextPos = -1;
		if (mapPool instanceof Rotation) {
			nextPos = ((Rotation) mapPool).getNextPosition() - 1;
			if (nextPos < 0) {
				nextPos = mapPool.getMaps().size() - 1;
			}
		}

		if (nextPos >= 0 && mapPool.getMaps().size() > nextPos &&
				mapPoolManager.getNextMap() != null) {
			MapInfo nextRotationMap = mapPool.getMaps().get(nextPos);
			MapInfo nextMap = mapPoolManager.getNextMap();
			if (!nextRotationMap.equals(nextMap)) {
				sender.sendMessage(ChatColor.RED + "Somebody else has recently SetNext a map. Try again soon");
				return;
			}
		}

		String poolName = mapPool.getName().toLowerCase();

		Map<String, Map<String, Set<String>>> rotations = StratusAPIPGM.get().getSetNextRotations().getRotations();
		Set<String> eligibleTokenNames = rotations.entrySet().stream()
				.filter(e -> e.getValue().containsKey(poolName) &&
						e.getValue().get(poolName).contains(mapInfo.getName()))
				.map(Map.Entry::getKey)
				.collect(Collectors.toSet());

        if(eligibleTokenNames.size() <= 0) {
            sender.sendMessage(ChatColor.RED + "Map is not eligible for your SetNext Tokens");
            return;
        }

        SetnextRequest request = new SetnextRequest(player.getUniqueId(), mapInfo.getName(), poolName);
		StratusAPI.get().newSharedChain("setnext").asyncFirst(() -> request.make(StratusAPI.get().getApiClient())
		).syncLast(response -> {
			order.setNextMap(mapInfo);
			new MultiAudience.Builder().global().build().sendMessage("setnext.success", player.getDisplayName(), mapInfo.getName());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "snmaps" },
			desc = "View the maps you can SetNext with a token")
	public void setNextMaps(@Sender CommandSender sender, @Default("1") int page) {
		int pageSize = 8;

		final PGM pgm = PGM.get();

		MapOrder order = PGM.get().getMapOrder();
		if (order == null || !(order instanceof MapPoolManager)) {
			sender.sendMessage(ChatColor.RED + "SetNext Tokens cannot be used on this server");
			return;
		}

		MapPoolManager mapPoolManager = (MapPoolManager) order;
		MapPool mapPool = mapPoolManager.getActiveMapPool();

		if (mapPool == null) {
			sender.sendMessage(ChatColor.RED + "SetNext Tokens cannot be used at this time");
			return;
		}

		String poolName = mapPool.getName().toLowerCase();

		Map<String, Map<String, Set<String>>> rotations = StratusAPIPGM.get().getSetNextRotations().getRotations();
		Set<String> eligibleTokenNames = rotations.entrySet().stream()
				.filter(e -> e.getValue().containsKey(poolName))
				.map(Map.Entry::getKey)
				.collect(Collectors.toSet());

		Map<String, Set<String>> allMaps = new HashMap<>();
		for (String token : eligibleTokenNames) {
			List<String> maps = new ArrayList<>(rotations.get(token).getOrDefault(poolName, Collections.emptySet()));
			for (String map : maps) {
				MapInfo mapInfo = pgm.getMapLibrary().getMap(map);
				if (mapInfo == null) {
					continue;
				}

				if (!allMaps.containsKey(map)) {
					allMaps.put(map, new HashSet<>());
				}

				allMaps.get(map).add(token);
			}
		}

		page = Math.max(1, page);
		int startIndex = (page - 1) * pageSize;
		if (allMaps.size() <= startIndex) {
			sender.sendMessage(ChatColor.RED + "No more pages left.");
			return;
		}

		sender.sendMessage("Page " + (page));
		List<String> sortedKeys = getPage(new ArrayList<>(new TreeSet<>(allMaps.keySet())), pageSize, page - 1);
		for (int i = 0; i < sortedKeys.size(); i++) {
			sender.sendMessage((startIndex + i + 1) + ". " + sortedKeys.get(i) +
					" - " + String.join(", ", allMaps.get(sortedKeys.get(i))));
		}
	}

	public <T> List<T> getPage(Collection<T> c, Integer pageSize, int page) {
		if (c == null)
			return Collections.emptyList();
		List<T> list = new ArrayList<T>(c);
		if (pageSize == null || pageSize <= 0 || pageSize > list.size())
			pageSize = list.size();
		return list.subList(page * pageSize, Math.min(++page * pageSize, list.size()));
	}
}
