/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.statistics;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.util.concurrent.AtomicDouble;
import network.stratus.api.pgm.unification.DropletsManager;
import network.stratus.api.pgm.unification.Droplets;
import network.stratus.api.pgm.unification.QuestCondition;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.models.QuestManager;
import network.stratus.api.pgm.tasks.PlaytimeTask;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.ParticipantState;
import tc.oc.pgm.core.Core;
import tc.oc.pgm.destroyable.Destroyable;
import tc.oc.pgm.flag.Flag;
import tc.oc.pgm.goals.TouchableGoal;
import tc.oc.pgm.score.ScoreConfig;
import tc.oc.pgm.score.ScoreMatchModule;
import tc.oc.pgm.stats.PlayerStats;
import tc.oc.pgm.stats.StatsMatchModule;
import tc.oc.pgm.wool.MonumentWool;

/**
 * Manages statistics for matches.
 *
 * @author Ian Ballingall
 *
 */
public class StatisticsManager {

	private MatchStatistics matchStatistics;
	private Map<UUID, BukkitTask> playtimeTasks;

	public StatisticsManager() {
		this.playtimeTasks = new ConcurrentHashMap<>();
	}

	/**
	 * Returns true iff there is currently a match running.
	 */
	public boolean isMatchActive() {
		return matchStatistics.isMatchActive();
	}

	/**
	 * Initialise a new statistics tracker for the match.
	 */
	public void newMatch() {
		System.out.println("New match loaded for statistics!");
		this.matchStatistics = new MatchStatistics();
	}

	/**
	 * Notify termination of the match, giving a requestable object for the match's
	 * statistics.
	 *
	 * @return The statistics for the match
	 */
	public MatchStatistics endMatch() {
		matchStatistics.setMatchActive(false);
		clearPlaytimeTasks();
		return matchStatistics;
	}

	/**
	 * Begins tracking statistics for this player.
	 *
	 * @param player The {@link UUID} of the player to track
	 */
	public void trackPlayer(UUID player) {
		matchStatistics.getTrackedPlayers().add(player);
	}

	/**
	 * Begins tracking statistics for the given collection of players.
	 *
	 * @param players The {@link Collection} of {@link UUID}s of players to track
	 */
	public void trackPlayers(Collection<UUID> players) {
		matchStatistics.getTrackedPlayers().addAll(players);
	}

	/**
	 * Obtain the statistics records for the current match.
	 *
	 * @return An object storing the statistcs for the current match
	 */
	public MatchStatistics getMatchStatistics() {
		return matchStatistics;
	}

	/**
	 * Increment the player's kill count by one.
	 *
	 * @param player The UUID of the player who made the kill
	 * @param match The current match
	 * @return The player's new kill count
	 */
	public int registerKill(UUID player, Match match) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger kills = matchStatistics.getKills().get(player);
		if (kills == null) {
			kills = new AtomicInteger();
			matchStatistics.getKills().put(player, kills);
		}

		int newKills = kills.incrementAndGet();

		registerKillDroplets(player, match, newKills);

		return newKills;
	}

	private void registerKillDroplets(UUID player, Match match, int newKills) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		StatsMatchModule smm = match.needModule(StatsMatchModule.class);
		PlayerStats stats = smm.getPlayerStat(player);

		AtomicDouble killsDroplets = matchStatistics.getDroplets().getKillsDroplets().get(player);
		if (killsDroplets == null) {
			killsDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getKillsDroplets().put(player, killsDroplets);
		}

		Droplets droplets = StratusAPIPGM.get().getDropletsManager().getDroplets();
		boolean isKillstreakDroplets = droplets.getKills().getKillStreakCounts().contains(newKills);

		double killStreakDroplets = isKillstreakDroplets ?
				stats.getKillstreak() * droplets.getKills().getKillStreakMultiplier() :
				droplets.getKills().getKill();

		String translation = isKillstreakDroplets ? "droplets.killstreak" : "droplets.kill";

		StratusAPIPGM.get().getDropletsManager().playEffect(killStreakDroplets, player, translation);

		killsDroplets.addAndGet(killStreakDroplets);
	}

	/**
	 * Increment the player's death count by one.
	 *
	 * @param player The UUID of the player who died
	 * @return The player's new death count
	 */
	public int registerDeath(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger deaths = matchStatistics.getDeaths().get(player);
		if (deaths == null) {
			deaths = new AtomicInteger();
			matchStatistics.getDeaths().put(player, deaths);
		}

		return deaths.incrementAndGet();
	}

	/**
	 * Increment the number of wools captured by one.
	 *
	 * @param player The UUID of the player who captured the wool
	 * @return The player's new wool capture count
	 */
	public int registerWool(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger wools = matchStatistics.getWools().get(player);
		if (wools == null) {
			wools = new AtomicInteger();
			matchStatistics.getWools().put(player, wools);
		}

		registerWoolDroplets(player);

		return wools.incrementAndGet();
	}

	private void registerWoolDroplets(UUID player) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble woolDroplets = matchStatistics.getDroplets().getWoolDroplets().get(player);
		if (woolDroplets == null) {
			woolDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getWoolDroplets().put(player, woolDroplets);
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		double count = droplets.getObjectives().getWoolCap().get(rotation);

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player, "droplets.wool.capture");

		woolDroplets.addAndGet(count);
	}

	public void registerWoolCarrierKillDroplets(UUID player) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble woolCarrierKillDroplets = matchStatistics.getDroplets().getWoolCarrierKillDroplets().get(player);
		if (woolCarrierKillDroplets == null) {
			woolCarrierKillDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getWoolCarrierKillDroplets().put(player, woolCarrierKillDroplets);
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		double count = droplets.getMiscellaneous().getKillWoolCarrier().get(rotation);

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player, "droplets.wool.killcarrier");

		woolCarrierKillDroplets.addAndGet(count);
	}

	public void registerWoolDestroyDroplets(UUID player) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble woolDestroyDroplets = matchStatistics.getDroplets().getWoolDestroyDroplets().get(player);
		if (woolDestroyDroplets == null) {
			woolDestroyDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getWoolDestroyDroplets().put(player, woolDestroyDroplets);
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		double count = droplets.getMiscellaneous().getThrowAwayWool().get(rotation);

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player, "droplets.wool.destroy");

		woolDestroyDroplets.addAndGet(count);
	}

	/**
	 * Increment the number of cores leaked by one.
	 *
	 * @param player The UUID of the player who leaked the core
	 * @return The player's new core leak count
	 */
	public int registerCore(UUID player, Core core) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger cores = matchStatistics.getCores().get(player);
		if (cores == null) {
			cores = new AtomicInteger();
			matchStatistics.getCores().put(player, cores);
		}

		registerCoreDroplets(player, core);

		return cores.incrementAndGet();
	}

	private void registerCoreDroplets(UUID player, Core core) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble coreDroplets = matchStatistics.getDroplets().getCoreDroplets().get(player);
		if (coreDroplets == null) {
			coreDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getCoreDroplets().put(player, coreDroplets);
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		Set<ParticipantState> touchingPlayers = core.getTouchingPlayers();
		double count = droplets.getObjectives().getCoreLeak().get(rotation) *
				(2.0 / (touchingPlayers.size() + 1.0));

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player, "droplets.core.leak");

		coreDroplets.addAndGet(count);
	}

	/**
	 * Increment the number of monuments broken by one.
	 *
	 * @param player The UUID of the player who broke the monument
	 * @return The player's new monument break count
	 */
	public int registerMonument(UUID player, double percentage) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger monuments = matchStatistics.getMonuments().get(player);
		if (monuments == null) {
			monuments = new AtomicInteger();
			matchStatistics.getMonuments().put(player, monuments);
		}

		registerMonumentDroplets(player, percentage);

		return monuments.incrementAndGet();
	}

	private void registerMonumentDroplets(UUID player, double percentage) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble monumentDroplets = matchStatistics.getDroplets().getMonumentDroplets().get(player);
		if (monumentDroplets == null) {
			monumentDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getMonumentDroplets().put(player, monumentDroplets);
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		double count = droplets.getObjectives().getMonumentBreak().get(rotation) * percentage;

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player, "droplets.monument.break");

		monumentDroplets.addAndGet(count);
	}

	/**
	 * Increment the number of scoreboxes scored
	 *
	 * @param player The UUID of the player who scored
	 */
	public int registerScorebox(MatchPlayer player, double score) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger scoreboxes = matchStatistics.getScoreboxes().get(player.getId());
		if (scoreboxes == null) {
			scoreboxes = new AtomicInteger();
			matchStatistics.getScoreboxes().put(player.getId(), scoreboxes);
		}

		registerScoreboxDroplets(player, score);

		return scoreboxes.incrementAndGet();
	}

	private void registerScoreboxDroplets(MatchPlayer player, double score) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		double pointMultiplier = droplets.getMiscellaneous().getScorebox().get("pointMultiplier");
		double defaultValue = dropletsManager.getDroplets().getMiscellaneous().getScorebox().get("default");
		ScoreMatchModule smm = player.getMatch().getModule(ScoreMatchModule.class);
		if (smm == null) {
			return;
		}

		AtomicDouble scoreboxDroplets = matchStatistics.getDroplets().getScoreboxDroplets().get(player.getId());
		if (scoreboxDroplets == null) {
			scoreboxDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getScoreboxDroplets().put(player.getId(), scoreboxDroplets);
		}

		ScoreConfig config = smm.getConfig();

		//Max 10 points
		double count = Math.min(10, config.killScore != 0 ?
				pointMultiplier * score :
				defaultValue);

		dropletsManager.playEffect(count, player.getBukkit(), "droplets.scorebox");

		scoreboxDroplets.addAndGet(count);
	}

	/**
	 * Add golden apple eating.
	 *
	 * @param player The UUID of the player who touched the objective
	 */
	public int registerGoldenAppleAte(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger goldenApplesEaten = matchStatistics.getGoldenApplesEaten().get(player);
		if (goldenApplesEaten == null) {
			goldenApplesEaten = new AtomicInteger();
			matchStatistics.getGoldenApplesEaten().put(player, goldenApplesEaten);
		}

		return goldenApplesEaten.incrementAndGet();
	}

	/**
	 * Add touches.
	 *
	 * @param player The UUID of the player who touched the objective
	 */
	public void registerTouch(ParticipantState player, TouchableGoal goal) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		UUID uuid = player.getId();

		//Add the touch
		Set<String> touches = matchStatistics.getTouches().getOrDefault(uuid, new HashSet<>());
		int oldSize = touches.size();
		touches.add(goal.getId());
		int newSize = touches.size();

		//Award unification if necessary
		if (newSize > oldSize) {
			QuestManager questManager = StratusAPIPGM.get().getQuestManager();
			if (questManager != null) {
				questManager.addProgress(player.getId(), QuestCondition.TOUCH_OBJECTIVE);
			}
			registerTouchDroplets(player, goal);
		}

		matchStatistics.getTouches().put(uuid, touches);
	}

	/**
	 * Add first touches.
	 *
	 * @param player The UUID of the player who touched the objective
	 */
	public void registerFirstTouch(ParticipantState player, TouchableGoal goal) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		UUID uuid = player.getId();

		//Add the touch
		Set<String> firstTouches = matchStatistics.getFirstTouches().getOrDefault(uuid, new HashSet<>());
		firstTouches.add(goal.getId());

		matchStatistics.getFirstTouches().put(uuid, firstTouches);
	}

	private void registerTouchDroplets(ParticipantState player, TouchableGoal goal) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble touchDroplets = matchStatistics.getDroplets().getTouchDroplets().get(player.getId());
		if (touchDroplets == null) {
			touchDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getTouchDroplets().put(player.getId(), touchDroplets);
		}

		Droplets droplets = StratusAPIPGM.get().getDropletsManager().getDroplets();

		//Get the objective's droplet count
		Map<String, Double> objectiveDroplets = null;
		StringBuilder translation = new StringBuilder("droplets.");
		if (goal instanceof MonumentWool) {
			objectiveDroplets = droplets.getObjectives().getWoolTouch();
			translation.append("wool");
		} else if (goal instanceof Core) {
			objectiveDroplets = droplets.getObjectives().getCoreTouch();
			translation.append("core");
		} else if (goal instanceof Flag) {
			objectiveDroplets = droplets.getObjectives().getFlagTouch();
			translation.append("flag");
		} else if (goal instanceof Destroyable) {
			objectiveDroplets = droplets.getObjectives().getMonumentTouch();
			translation.append("monument");
		}
		translation.append(".touch");

		//Play the droplet effect for the right count
		double count = objectiveDroplets != null ?
				objectiveDroplets.getOrDefault(rotation, 0d) :
				0d;

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player.getId(), translation.toString());

		touchDroplets.addAndGet(count);
	}

	/**
	 * Increment the number of flags captured by one.
	 *
	 * @param player The UUID of the player who captured the flag
	 * @return The player's new flag capture count
	 */
	public int registerFlag(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger flags = matchStatistics.getFlags().get(player);
		if (flags == null) {
			flags = new AtomicInteger();
			matchStatistics.getFlags().put(player, flags);
		}

		registerFlagDroplets(player);

		return flags.incrementAndGet();
	}

	private void registerFlagDroplets(UUID player) {
		if (!StratusAPIPGM.get().isDropletsEnabled()) {
			return;
		}

		String rotation = StratusAPIPGM.get().getDropletsRotation();

		AtomicDouble flagDroplets = matchStatistics.getDroplets().getFlagDroplets().get(player);
		if (flagDroplets == null) {
			flagDroplets = new AtomicDouble();
			matchStatistics.getDroplets().getFlagDroplets().put(player, flagDroplets);
		}

		DropletsManager dropletsManager = StratusAPIPGM.get().getDropletsManager();
		Droplets droplets = dropletsManager.getDroplets();

		double count = droplets.getObjectives().getFlagCap().get(rotation);

		StratusAPIPGM.get().getDropletsManager().playEffect(count, player, "droplets.flag.capture");

		flagDroplets.addAndGet(count);
	}

	public void registerEvent(String event) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		matchStatistics.getEvents().add(event);
	}

	public int registerQuestProgress(UUID uuid, String condition, int count) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		//Add the touch
		Map<String, AtomicInteger> progress = matchStatistics.getQuestProgress()
				.computeIfAbsent(uuid, k -> new HashMap<>());

		AtomicInteger newProgress = progress.get(condition);
		if (newProgress == null) {
			newProgress = new AtomicInteger();
			progress.put(condition, newProgress);
		}

		int returnValue = newProgress.addAndGet(count);

		matchStatistics.getQuestProgress().put(uuid, progress);

		return returnValue;
	}

	/**
	 * Sends the given player a summary of their match statistics.
	 *
	 * @param player The player to send a summary
	 */
	public void sendMatchStatisticsSummary(Player player, Match match) {
		if (match.isRunning() || !match.isFinished()) {
			return;
		}
		UUID uuid = player.getUniqueId();
		int kills = matchStatistics.getKills(uuid);
		int deaths = matchStatistics.getDeaths(uuid);
		int wools = matchStatistics.getWools(uuid);
		int cores = matchStatistics.getCores(uuid);
		int monuments = matchStatistics.getMonuments(uuid);
		int flags = matchStatistics.getFlags(uuid);
		double kdr = (deaths == 0) ? (double) kills : (double) kills / deaths;
		String kdrString = String.format("%.2f", kdr);

		player.sendMessage(ChatColor.STRIKETHROUGH + "--------------" +
		ChatColor.RESET + ChatColor.AQUA + ChatColor.BOLD + " Match Stats " +
				ChatColor.RESET + ChatColor.STRIKETHROUGH + "-------------- ");
		player.sendMessage("Kills: " + ChatColor.GREEN + kills +
				ChatColor.GRAY + " | " + ChatColor.RESET + "Deaths: " + ChatColor.RED + deaths +
				ChatColor.GRAY + " | " + ChatColor.RESET + "KDR: " + ChatColor.AQUA + kdrString);
		StringBuilder sb = new StringBuilder();
		if (wools > 0) {
			sb.append(ChatColor.RESET).append("Wools: ")
					.append(ChatColor.GOLD).append(wools)
					.append(ChatColor.GRAY).append(" | ");
		}
		if (cores > 0) {
			sb.append(ChatColor.RESET).append("Cores: ")
					.append(ChatColor.GOLD).append(cores)
					.append(ChatColor.GRAY).append(" | ");
		}
		if (flags > 0) {
			sb.append(ChatColor.RESET).append("Flags: ")
					.append(ChatColor.GOLD).append(flags)
					.append(ChatColor.GRAY).append(" | ");
		}
		if (monuments > 0) {
			sb.append(ChatColor.RESET).append("Monuments: ")
					.append(ChatColor.GOLD).append(monuments)
					.append(ChatColor.GRAY).append(" | ");
		}
		String objects = sb.toString();
		if (!objects.equalsIgnoreCase("")) {
			objects = objects.substring(0, objects.length() - 2);
			player.sendMessage(objects);
		}

		StatsMatchModule smm = match.needModule(StatsMatchModule.class);
		if (smm != null) {
			PlayerStats playerStats = smm.getPlayerStat(player.getUniqueId());
			if (playerStats != null) {
				player.sendMessage("Damage Dealt: " + ChatColor.GREEN + (int)(playerStats.getDamageDone() / 2) + "❤" +
						ChatColor.GRAY + " (" + (int)(playerStats.getBowDamage() / 2) + "➹) " +
						ChatColor.RESET + "Damage Taken: " + ChatColor.RED + (int)(playerStats.getDamageTaken() / 2) + "❤" +
						ChatColor.GRAY + " (" + (int)(playerStats.getBowDamageTaken() / 2) + "➹)");
			}

			Map<UUID, Integer> allKills = new HashMap<>();
			Map<UUID, Integer> allKillstreaks = new HashMap<>();
			Map<UUID, Double> allDamage = new HashMap<>();

			for (Map.Entry<UUID, PlayerStats> mapEntry : smm.getAllPlayerStats().entrySet()) {
				UUID playerUUID = mapEntry.getKey();
				PlayerStats mapPlayerStats = mapEntry.getValue();
				smm.getPlayerStat(playerUUID);
				allKills.put(playerUUID, mapPlayerStats.getKills());
				allKillstreaks.put(playerUUID, mapPlayerStats.getMaxKillstreak());
				allDamage.put(playerUUID, mapPlayerStats.getDamageDone());
			}

			Map.Entry<UUID, Integer> topKiller =
					allKills.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue)).orElse(null);

			Map.Entry<UUID, Integer> topKillstreak =
					allKillstreaks.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue)).orElse(null);

			Map.Entry<UUID, Double> topDamage =
					allDamage.entrySet().stream().max(Comparator.comparingDouble(Map.Entry::getValue)).orElse(null);

			if (topKiller != null) {
				Player top = Bukkit.getPlayer(topKiller.getKey());
				if (top != null && top.isOnline()) {
					player.sendMessage("Top Killer: " + ChatColor.DARK_GREEN + topKiller.getValue() +
							ChatColor.RESET + " by " + top.getDisplayName());
				}
			}

			if (topKillstreak != null) {
				Player top = Bukkit.getPlayer(topKillstreak.getKey());
				if (top != null && top.isOnline()) {
					player.sendMessage("Top Killstreak: " + ChatColor.DARK_GREEN + topKillstreak.getValue() +
							ChatColor.RESET + " by " + top.getDisplayName());
				}
			}

			if (topDamage != null) {
				Player top = Bukkit.getPlayer(topDamage.getKey());
				if (top != null && top.isOnline()) {
					player.sendMessage("Top DMG Dealt: " + ChatColor.DARK_GREEN + (topDamage.getValue().intValue() / 2) + "❤" +
							ChatColor.RESET + " by " + top.getDisplayName());
				}
			}
		}

		player.sendMessage(ChatColor.GOLD + "Stats: " + ChatColor.AQUA + ChatColor.ITALIC + "https://stratus.network/m/" + matchStatistics.getMatchId());
		player.sendMessage(ChatColor.STRIKETHROUGH + "------------------------------------------");


//		SingleAudience audience = new SingleAudience(player);
//		audience.sendMessage("statistics.summary");
//		audience.sendMessage("statistics.view.killsdeaths", kills, deaths,
//				(deaths == 0) ? (float) kills : (float) kills / deaths);
//		audience.sendMessage("statistics.view.objectives.title");
//		audience.sendMessage("statistics.view.objectives", wools, cores, monuments, flags);

	}

	public void sendMatchStatisticsSummary(UUID uuid, Match match) {
		Player player = StratusAPIPGM.get().getServer().getPlayer(uuid);
		if (player != null)
			sendMatchStatisticsSummary(player, match);
	}

	/**
	 * Start a new task to track a given player's playtime. If one already exists,
	 * it will be cancelled and replaced with the new task.
	 *
	 * @param uuid  The player's UUID
	 * @param match The match the player is in
	 */
	public void newPlaytimeTask(UUID uuid, Match match) {
		matchStatistics.getPlaytime().putIfAbsent(uuid, new HashMap<>());
		BukkitTask task = new PlaytimeTask(uuid, matchStatistics.getPlaytime().get(uuid), match)
				.runTaskTimerAsynchronously(StratusAPIPGM.get(), 20, 20);
		BukkitTask oldTask = playtimeTasks.put(uuid, task);
		if (oldTask != null) {
			oldTask.cancel();
		}
	}

	/**
	 * Remove a player's playtime tracking task from the list and cancel it, if one
	 * exists.
	 *
	 * @param uuid The player's UUID
	 */
	public void removePlaytimeTask(UUID uuid) {
		BukkitTask oldTask = playtimeTasks.remove(uuid);
		if (oldTask != null) {
			oldTask.cancel();
		}
	}

	/**
	 * Cancel all active playtime tasks and reset the map.
	 */
	private void clearPlaytimeTasks() {
		playtimeTasks.forEach((uuid, task) -> task.cancel());
		playtimeTasks.clear();
	}

}
