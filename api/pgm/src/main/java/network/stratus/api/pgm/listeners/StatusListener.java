/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import network.stratus.api.pgm.status.PGMStatusUpdater;
import tc.oc.pgm.api.match.event.MatchPhaseChangeEvent;
import tc.oc.pgm.events.PlayerPartyChangeEvent;

/**
 * Listens to events that means we need to send a PGM status update sooner.
 */
public class StatusListener implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onMatchPhaseChange(final MatchPhaseChangeEvent event) {
        PGMStatusUpdater.get().markDirty();
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerPartyChange(final PlayerPartyChangeEvent event) {
        PGMStatusUpdater.get().markDirty();
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerQuit(final PlayerQuitEvent event) {
        PGMStatusUpdater.get().markDirty();
    }

}
