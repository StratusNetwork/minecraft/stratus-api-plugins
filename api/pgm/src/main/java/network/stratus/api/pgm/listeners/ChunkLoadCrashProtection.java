/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchUnloadEvent;
import tc.oc.pgm.events.PlayerJoinMatchEvent;
import tc.oc.pgm.spawns.SpawnMatchModule;

/**
 * Protects against server crashes caused by excessive chunk generation.
 * 
 * @author Ian Ballingall
 *
 */
public class ChunkLoadCrashProtection implements Listener {

	private static final int MAX_RADIUS_SQUARED = (int) Math.pow(1000, 2);

	private Map<Match, Location> spawnLocations = new HashMap<>();

	@EventHandler(priority = EventPriority.MONITOR)
	public void onSpawn(PlayerJoinMatchEvent event) {
		if (spawnLocations.containsKey(event.getMatch()) || !event.getNewParty().isObserving())
			return;

		Location spawn = event.getMatch().getModule(SpawnMatchModule.class).getDefaultSpawn()
				.getSpawn(event.getPlayer());
		spawnLocations.put(event.getMatch(), spawn);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchUnload(MatchUnloadEvent event) {
		spawnLocations.remove(event.getMatch());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerMove(PlayerMoveEvent event) {
		Location spawn = spawnLocations.get(PGM.get().getMatchManager().getMatch(event.getWorld()));
		if (spawn == null || !PGM.get().getMatchManager().getPlayer(event.getPlayer()).isObserving())
			return;

		if (event.getTo().distanceSquared(spawn) > MAX_RADIUS_SQUARED) {
			event.setTo(spawn);
		}
	}

}
