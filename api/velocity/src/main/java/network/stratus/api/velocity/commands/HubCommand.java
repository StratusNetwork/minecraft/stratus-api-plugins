/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.commands;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.velocitypowered.api.command.Command;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.command.SimpleCommand;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.server.RegisteredServer;

import network.stratus.api.velocity.chat.SingleAudience;

/**
 * A command to send the player back to the lobby. Presently supports a single
 * lobby server.
 *
 * @author Ian Ballingall
 *
 */
public class HubCommand implements SimpleCommand {

	private final RegisteredServer lobbyServer;

	public HubCommand(RegisteredServer registeredServer) {
		this.lobbyServer = registeredServer;
	}

	@Override
	public void execute(final Invocation invocation) {
		CommandSource source = invocation.source();
		if (!(source instanceof Player)) {
			new SingleAudience(source).sendMessage("command.error.notplayer");
			return;
		}

		Player player = (Player) source;
		player.getCurrentServer().ifPresent(server -> {
			if (server.getServer().equals(lobbyServer)) {
				new SingleAudience(source).sendMessage("hub.error.alreadyinhub");
			} else {
				player.createConnectionRequest(lobbyServer).fireAndForget();
			}
		});
	}

}
