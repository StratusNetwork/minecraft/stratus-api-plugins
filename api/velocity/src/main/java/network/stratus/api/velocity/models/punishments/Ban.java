/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.models.punishments;

import java.util.Date;

import net.kyori.adventure.text.Component;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.PunishmentFactory.Type;
import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Encapsulates a ban issued against a player.
 *
 * @author Ian Ballingall
 *
 */
public class Ban extends VelocityPunishment {

	public Ban(String _id, User issuer, User target, Date time, Date expiry, String reason, boolean active, int number,
			String serverName, boolean silent) {
		super(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
	}

	/**
	 * Enforces the punishment. Presently, this is not broadcast to anyone, as it is
	 * expected that individual servers will handle the broadcasting, rendering
	 * display names accordingly.
	 */
	@Override
	public void enforce(boolean showServer) {
		targetPlayer.ifPresent(player -> {
			final String message;
			if (expiry == null) {
				message = String.format(StratusAPIVelocity.get().getTranslator().getStringOrDefaultLocale(
						player.getPlayerSettings().getLocale(), "punishment.ban.permanent.message"), reason);
			} else {
				message = String.format(
						StratusAPIVelocity.get().getTranslator().getStringOrDefaultLocale(
								player.getPlayerSettings().getLocale(), "punishment.ban.temporary.message"),
						reason, StratusAPIVelocity.get().getTranslator()
								.getTimeDifferenceString(player.getPlayerSettings().getLocale(), expiry, time));
			}

			player.disconnect(Component.text(message));
		});
	}

	@Override
	public Type getType() {
		return Type.BAN;
	}

}
