/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.messaging;

import com.velocitypowered.api.proxy.server.ServerInfo;
import network.stratus.api.messaging.MessageProcessor;
import network.stratus.api.velocity.StratusAPIVelocity;

import java.net.InetSocketAddress;

/**
 * Processes a player count message received from other proxies.
 * 
 * @author Ian Ballingall
 *
 */
public class ModifyServerMessageProcessor implements MessageProcessor<ModifyServer> {

	private String proxyName;

	public ModifyServerMessageProcessor(String proxyName) {
		this.proxyName = proxyName;
	}

	@Override
	public void process(ModifyServer object) {
        StratusAPIVelocity.get().getLogger()
				.info("Incoming server: " + object.getName() + "," + object.getIp() + "," + object.getProxy());
		ServerInfo info = new ServerInfo(object.getName(), new InetSocketAddress(object.getIp(), object.getPort()));

		if (object.isAdd()) {
			StratusAPIVelocity.get().getServer().registerServer(info);
		} else {
			StratusAPIVelocity.get().getServer().unregisterServer(info);
		}
	}
}
