/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

import java.util.UUID;

/**
 * A request to create a new proxy.
 *
 * @author ShinyDialga
 *
 */
public class ProxyCreateRequest implements Request<Void> {

	private String name;
	private UUID sender;
	private String size;

	public ProxyCreateRequest(String name, UUID sender, String size) {
		this.name = name;
		this.sender = sender;
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public UUID getSender() {
		return sender;
	}

	public String getSize() {
		return size;
	}

	@Override
	public String getEndpoint() {
		return "/servers/proxy/create";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Void make(APIClient client) {
		return client.post(this);
	}

}
