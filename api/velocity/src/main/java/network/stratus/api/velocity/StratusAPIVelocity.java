/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import network.stratus.api.velocity.commands.ServerCommands;
import network.stratus.api.velocity.messaging.ModifyServer;
import network.stratus.api.velocity.messaging.ModifyServerMessageProcessor;
import network.stratus.api.velocity.messaging.SendChat;
import network.stratus.api.velocity.messaging.SendChatMessageProcessor;
import network.stratus.api.velocity.messaging.ServerPlayerCount;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;

import com.google.inject.Inject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.velocitypowered.api.command.CommandManager;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.scheduler.ScheduledTask;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.JAXRSClient;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.i18n.DefaultLocaleTranslator;
import network.stratus.api.i18n.LanguageManager;
import network.stratus.api.i18n.Translator;
import network.stratus.api.messaging.JsonConsumer;
import network.stratus.api.messaging.JsonPublisher;
import network.stratus.api.messaging.RabbitMessenger;
import network.stratus.api.models.server.ServerStatus;
import network.stratus.api.requests.ServerStatusChangeRequest;
import network.stratus.api.responses.ServerResponse;
import network.stratus.api.velocity.commands.HubCommand;
import network.stratus.api.velocity.listeners.LoginListener;
import network.stratus.api.velocity.listeners.PingListener;
import network.stratus.api.velocity.messaging.ConsumerExceptionHandler;
import network.stratus.api.velocity.messaging.PlayerCount;
import network.stratus.api.velocity.messaging.PlayerCountMessageProcessor;
import network.stratus.api.velocity.proxy.PlayerCountManager;
import network.stratus.api.velocity.tasks.PlayerCountBroadcastTask;

/**
 * The main class for the Velocity plugin.
 *
 * @author Ian Ballingall
 *
 */
@Plugin(id = "stratus-api", name = "Stratus-API-Velocity", version = "7.1.0-SNAPSHOT", description = "Stratus API plugin for Velocity", authors = {
		"Stratus Network", "Ian Ballingall" }) // If you work on this, feel free to add your name :D
public class StratusAPIVelocity {

	private static StratusAPIVelocity plugin;

	private final ProxyServer server;
	private final Logger logger;
	private final CommandManager commands;

	private String proxyName;

	private Configuration defaultConfig;
	private APIClient apiClient;
	private Translator translator;
	private RabbitMessenger messenger;

	private PlayerCountManager playerCountManager;
	private JsonPublisher playerCountPublisher;
	private JsonPublisher serverPlayerCountPublisher;

	@Inject
	public StratusAPIVelocity(ProxyServer server, Logger logger, CommandManager commands)
			throws IOException, ConfigurationException {
		plugin = this;

		this.server = server;
		this.logger = logger;
		this.commands = commands;

		this.defaultConfig = loadDefaultConfiguration();

		String apiClientUrl = defaultConfig.getString("api.url");
		String authToken = defaultConfig.getString("api.authToken");
		apiClient = new JAXRSClient.Builder(apiClientUrl).authToken(authToken).build();

		if (defaultConfig.getBoolean("messaging.enabled", false)) {
			messenger = new RabbitMessenger(defaultConfig.getString("messaging.uri"), new ConsumerExceptionHandler());
			try {
				messenger.initialiseConnection();
			} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException | IOException
					| TimeoutException | UnrecoverableKeyException | KeyStoreException | CertificateException e) {
				logger.error("Failed to connect to RabbitMQ server: " + e);
				logger.error("Messaging services will be disabled");
				e.printStackTrace();
				messenger = null;
			}
		}

		translator = new DefaultLocaleTranslator(
				new LanguageManager.Builder(Locale.UK).loadExternally("./i18n").loadProperties().build());

		playerCountManager = new PlayerCountManager();

		logger.info("Stratus API Velocity enabled");
	}

	/**
	 * Called after the proxy has initialised. Anything dependent on the proxy or
	 * its servers should be done in here as they will not be available on
	 * initialisation.
	 */
	@Subscribe
	public void onProxyInitialization(ProxyInitializeEvent event) {
		final ServerResponse serverData;
		try {
			serverData = new ServerStatusChangeRequest(ServerStatus.ONLINE).make(apiClient);
		} catch (RequestFailureException e) {
			e.printStackTrace();
			logger.error("Failed to connect to backend, API services will be unavailable");
			e.getResponse().ifPresent(response -> logger.error(response.getDescription()));
			return;
		}

		proxyName = serverData.getName();

		System.out.println("This should NOT be a date!!! ${date:YYYY-mm-dd}");

		Optional<RegisteredServer> lobbyServer = server.getServer(defaultConfig.getString("lobbyServer", "lobby"));
		if (lobbyServer.isPresent()) {
			// commands.register("hub", new HubCommand(lobbyServer.get()), "lobby");
		} else {
			logger.warn("Failed to find lobby server, skipping hub command registration");
		}

		commands.register("editbungeeserver", new ServerCommands.ServerEditCommand());
		// commands.register("personal", new ServerCommands.PersonalCreateCommand());
		// commands.register("createserver", new ServerCommands.ServerCreateCommand());
		// commands.register("createproxy", new ServerCommands.ProxyCreateCommand());
		// commands.register("ranked", new ServerCommands.RankedCommand());

		configureMessagingServices();

		server.getEventManager().register(this, new LoginListener(defaultConfig.getString("registration.host", null)));
		server.getEventManager().register(this, new PingListener(playerCountManager));
	}

	/**
	 * Called on proxy shutdown. Performs necessary clean-up and notifies backend of
	 * shutdown.
	 */
	@Subscribe
	public void onProxyShutdown(ProxyShutdownEvent event) {
		plugin = null;

		if (playerCountManager.getPublishTask() != null) {
			playerCountManager.getPublishTask().cancel();
			try {
				playerCountPublisher.publish(new PlayerCount(proxyName, 0, true));
				for (RegisteredServer server : getServer().getAllServers()) {
					serverPlayerCountPublisher.publish(new ServerPlayerCount(proxyName, server.getServerInfo().getName(), 0, true));
				}
			} catch (IOException e) {
				logger.error("Failed to destroy player count: " + e);
				e.printStackTrace();
			}
		}

		try {
			new ServerStatusChangeRequest(ServerStatus.OFFLINE).make(apiClient);
		} catch (RequestFailureException e) {
			e.printStackTrace();
			logger.warn("Failed to notify backend of server shutdown");
			e.getResponse().ifPresent(response -> logger.warn(response.getDescription()));
		}
	}

	/**
	 * Get the currently active plugin instance.
	 *
	 * @return The plugin instance
	 */
	public static StratusAPIVelocity get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

	/**
	 * Get the Velocity {@link ProxyServer} instance this plugin is associated with.
	 *
	 * @return The Velocity instance
	 */
	public ProxyServer getServer() {
		return server;
	}

	/**
	 * Get the {@link Logger} for logging information.
	 *
	 * @return The logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Get the proxy name as configured in the backend.
	 *
	 * @return The proxy name
	 */
	public String getProxyName() {
		return proxyName;
	}

	/**
	 * Get the default config loaded from config.yml.
	 *
	 * @return The default {@link Configuration}
	 */
	public Configuration getDefaultConfig() {
		return defaultConfig;
	}

	/**
	 * Get the configured {@link APIClient} for this plugin instance.
	 *
	 * @return The client
	 */
	public APIClient getApiClient() {
		return apiClient;
	}

	/**
	 * Get the currently active {@link Translator} for the plugin.
	 *
	 * @return The translator
	 */
	public Translator getTranslator() {
		return translator;
	}

	/**
	 * Get the {@link RabbitMessenger} object, through which asynchronous messaging
	 * can be done.
	 *
	 * @return The messenger
	 */
	public RabbitMessenger getMessenger() {
		return messenger;
	}

	/**
	 * Get the {@link PlayerCountManager}, which manages total player counts across
	 * multiple proxies.
	 *
	 * @return The player count manager
	 */
	public PlayerCountManager getPlayerCountManager() {
		return playerCountManager;
	}

	/**
	 * Loads in the default configuration from plugins/Stratus-API/config.yml.
	 * Sadly, Velocity doesn't seem to have a nice config system like Bukkit, so
	 * we're using Apache Commons Configuration2 to handle it all for us.
	 *
	 * @return The {@link Configuration} loaded from config.yml
	 * @throws IOException            If the config file cannot be read or written
	 * @throws ConfigurationException If there is an error loading or parsing the
	 *                                config file
	 */
	private Configuration loadDefaultConfiguration() throws IOException, ConfigurationException {
		File file = new File("plugins/Stratus-API/config.yml");
		logger.info("Loading default configuration from " + file.getAbsolutePath());
		if (!file.exists()) {
			logger.warn("Configuration does not exist!");
			saveDefaultConfiguration();
		}

		FileBasedConfigurationBuilder<YAMLConfiguration> builder = new FileBasedConfigurationBuilder<>(
				YAMLConfiguration.class).configure(new Parameters().fileBased().setFile(file));
		return builder.getConfiguration();
	}

	/**
	 * Saves the default configuration file. This should only be called if it
	 * doesn't already exist. The default file is found in resource/config.yml.
	 *
	 * @throws IOException            If there is an error reading or writing the
	 *                                config file or default config file
	 * @throws ConfigurationException If there is an error loading or parsing the
	 *                                default config file, or writing the new config
	 *                                file
	 */
	private void saveDefaultConfiguration() throws IOException, ConfigurationException {
		File directory = new File("plugins/Stratus-API/");
		logger.info("Saving default configuration to " + directory.getAbsolutePath());
		if (!directory.exists()) {
			directory.mkdir();
		}

		File file = new File("plugins/Stratus-API/config.yml");
		file.createNewFile();

		InputStream defaultIn = getClass().getResourceAsStream("/config.yml");
		YAMLConfiguration defaultYaml = new YAMLConfiguration();
		defaultYaml.read(defaultIn);
		defaultYaml.write(new FileWriter(file));
		defaultIn.close();
	}

	/**
	 * Configure asynchronous messaging services using the {@link RabbitMessenger}.
	 */
	private void configureMessagingServices() {
		if (messenger == null)
			return;

		if (defaultConfig.getBoolean("messaging.services.playerCounts.enabled", false)) {
			try {
				String routingKey = "proxy." + defaultConfig.getString("messaging.services.playerCounts.channel");
				new JsonConsumer.Builder<PlayerCount>(messenger.getNewChannel(), PlayerCount.class)
						.consumerName("playercount").exchangeName("playercount").exchangeType(BuiltinExchangeType.TOPIC)
						.routingKey(routingKey).messageProcessor(new PlayerCountMessageProcessor(proxyName)).build()
						.register();

				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("playercount", BuiltinExchangeType.TOPIC);
				playerCountPublisher = new JsonPublisher(publisherChannel, "playercount", routingKey,
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());

				Channel serverPublisherChannel = messenger.getNewChannel();
				serverPublisherChannel.exchangeDeclare("serverplayercount", BuiltinExchangeType.TOPIC);
				serverPlayerCountPublisher = new JsonPublisher(serverPublisherChannel, "serverplayercount", routingKey,
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());

				ScheduledTask publishTask = server.getScheduler()
						.buildTask(this, new PlayerCountBroadcastTask(this, proxyName, playerCountPublisher, serverPlayerCountPublisher))
						.repeat(defaultConfig.getLong("messaging.services.playerCounts.publishInterval"),
								TimeUnit.SECONDS)
						.delay(defaultConfig.getLong("messaging.services.playerCounts.publishInterval"),
								TimeUnit.SECONDS)
						.schedule();
				playerCountManager.setPublishTask(publishTask);

				ScheduledTask cleanupTask = server.getScheduler()
						.buildTask(this, playerCountManager.new Cleanup(
								defaultConfig.getLong("messaging.services.playerCounts.publishInterval") * 1000 * 5))
						.repeat(defaultConfig.getLong("messaging.services.playerCounts.publishInterval"),
								TimeUnit.SECONDS)
						.delay(defaultConfig.getLong("messaging.services.playerCounts.publishInterval"),
								TimeUnit.SECONDS)
						.schedule();
				playerCountManager.setCleanupTask(cleanupTask);
			} catch (IOException e) {
				logger.error("Failed to configure player count broadcast service: " + e);
				e.printStackTrace();
			}
		}

		if (defaultConfig.getBoolean("messaging.services.servers.enabled", false)) {
			try {
				new JsonConsumer.Builder<ModifyServer>(messenger.getNewChannel(), ModifyServer.class)
						.consumerName("servers").exchangeName("proxy.servers").exchangeType(BuiltinExchangeType.TOPIC)
						.messageProcessor(new ModifyServerMessageProcessor(proxyName)).build()
						.register();
			} catch (IOException e) {
				logger.error("Failed to configure servers servers service: " + e);
				e.printStackTrace();
			}
		}

		if (defaultConfig.getBoolean("messaging.services.sendChat.enabled", false)) {
			try {
				new JsonConsumer.Builder<SendChat>(messenger.getNewChannel(), SendChat.class)
						.consumerName("sendchat").exchangeName("proxy.sendchat").exchangeType(BuiltinExchangeType.TOPIC)
						.messageProcessor(new SendChatMessageProcessor()).build()
						.register();
			} catch (IOException e) {
				logger.error("Failed to configure servers sendChat service: " + e);
				e.printStackTrace();
			}
		}
	}

}
