package network.stratus.api.events.messaging;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Server;

import com.google.common.collect.Iterators;

import dev.pgm.events.EventsPlugin;
import dev.pgm.events.TournamentManager;
import dev.pgm.events.format.TournamentState;
import network.stratus.api.events.StratusAPIEvents;
import network.stratus.api.messaging.MessageProcessor;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;

public class FindSeriesServerMessageProcessor implements MessageProcessor<FindSeriesServer> {

    public FindSeriesServerMessageProcessor() {}

    /**
     * Logs a message about starting the specified match.
     */
    private void log(FindSeriesServer object, String message) {
        System.out.println("[TM " + object.getSeriesID() + "] " + message);
    }

    /**
     * Logs the reason the match can't start.
     */
    private void reject(FindSeriesServer object, String consoleError) {
        log(object, "Rejected find - " + consoleError);
    }

    @Override
    public void process(FindSeriesServer object) {
        log(object, "Being queried about finding a server");

        if (!StratusAPIEvents.get().getRemote()) {
            reject(object, "remote starts not supported here");
            return;
        }

        String serverName = StratusAPIEvents.get().getServerName().toLowerCase();
        if (!object.getServer().equals("") && !object.getServer().toLowerCase().equals(serverName)) {
            reject(object, "server name \""
                + StratusAPIEvents.get().getServerName()
                + "\" does not match requested server name \""
                + object.getServer()
                + "\""
            );
            return;
        }

        String group = StratusAPIEvents.get().getGroup().toLowerCase();
        if (!object.getGroup().equals("") && !object.getGroup().toLowerCase().equals(group)) {
            reject(object, "group \""
                + StratusAPIEvents.get().getGroup()
                + "\" does not match requested group \""
                + object.getGroup()
                + "\""
            );
            return;
        }

        // If we asked for this specific server,
        // go along with it for now so that we can reject it for a more specific reason later.
        // Otherwise, we bail out early.
        if (object.getServer().equals("")) {
            if (StratusAPIEvents.get().hasSeriesReady()) {
                reject(object, "series already queued");
                return;
            }

            Match currentMatch = Iterators.getNext(PGM.get().getMatchManager().getMatches(), null);
            if (currentMatch != null && currentMatch.isRunning()) {
                reject(object, "some match is already running");
            }

            TournamentManager tm = EventsPlugin.get().getTournamentManager();
            if (!tm.currentTournament()
                .map((tournament) -> tournament.state().equals(TournamentState.FINISHED))
                .orElse(true)
            ) {
                reject(object, "match already in progress");
                return;
            }
        }

        log(object, "Accepting find!");

        try {
            // Use original cased name for display purposes
            StratusAPIEvents.get().getFoundSeriesServerPublisher().publish(new FoundSeriesServer(object.getSeriesID(), StratusAPIEvents.get().getServerName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
