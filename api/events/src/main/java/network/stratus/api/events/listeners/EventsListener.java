/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.events.listeners;

import dev.pgm.events.api.events.TournamentFinishedEvent;
import dev.pgm.events.team.TournamentTeam;
import network.stratus.api.events.StratusAPIEvents;
import network.stratus.api.pgm.StratusAPIPGM;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import tc.oc.pgm.api.match.event.MatchFinishEvent;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.party.Competitor;

import java.util.Iterator;

public class EventsListener implements Listener {

	@EventHandler(priority = EventPriority.LOW)
	public void onMatchLoad(MatchLoadEvent event) {
		StratusAPIEvents.get().startSeriesIfPossible(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRoundFinished(MatchFinishEvent event) {
		//tie
		System.out.println(event.getWinners().size());
		String id = StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().getMatchId();
		if (event.getWinners().size() != 1) {
			Iterator<Competitor> iterator = event.getMatch().getCompetitors().iterator();
			String team1 = iterator.next().getNameLegacy();
			String team2 = iterator.next().getNameLegacy();
			StratusAPIEvents.get().postWebhook(team1 + " tied " + team2 + " on " + event.getMatch().getMap().getName() +
					" https://stratus.network/m/" + id);
//			StratusAPIEvents.get().postWebhook("!tm event add tie, " + team1 + ", " + team2);
			return;
		}

		Competitor winner = event.getWinners().iterator().next();
		Competitor loser = event.getMatch().getCompetitors().stream().filter(c -> !c.equals(winner)).findAny().orElse(null);
		String team1 = winner.getNameLegacy();
		String team2 = loser.getNameLegacy();
		StratusAPIEvents.get().postWebhook(team1 + " beat " + team2 + " on " + event.getMatch().getMap().getName() +
				" https://stratus.network/m/" + id);
//		StratusAPIEvents.get().postWebhook("!tm event add win, " + team1 + ", " + team2);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onTournamentFinished(TournamentFinishedEvent event) {
		if (!event.winningTeam().isPresent()) {
			try {
				Iterator<? extends TournamentTeam> teams = event.format().teamManager().teams().iterator();
				String team1 = teams.next().getName();
				String team2 = teams.next().getName();
				StratusAPIEvents.get().postWebhook(team1 + " has tied the series against " + team2);
//				StratusAPIEvents.get().postWebhook("!tm event add tie, " + team1 + ", " + team2);
			} catch (Exception e) {
				e.printStackTrace();
				StratusAPIEvents.get().postWebhook("An error occurred posting results for a tm");
			}

			return;
		}

		try {
			String winner = event.winningTeam().get().getName();
			String loser = event.losingTeam().isPresent() ?
                    event.losingTeam().get().getName() :
                    "null";
//			StratusAPIEvents.get().postWebhook("!tm event add win, " + winner + ", " + loser);
			StratusAPIEvents.get().postWebhook(winner + " has won the series against " + loser + "!");
		} catch (Exception e) {
			e.printStackTrace();
			StratusAPIEvents.get().postWebhook("An error occurred posting results for a tm");
		}
	}

}
