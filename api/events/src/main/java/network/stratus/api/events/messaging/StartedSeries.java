package network.stratus.api.events.messaging;

public class StartedSeries {
    private String seriesID;
    private String error;

    public StartedSeries() {

    }

    public StartedSeries(String seriesID, String error) {
        this.seriesID = seriesID;
        this.error = error;
    }

    public String getSeriesID() {
        return seriesID;
    }

    public String getError() {
        return error;
    }
}
