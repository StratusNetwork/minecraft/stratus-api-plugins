package network.stratus.api.events.messaging;

public class StartSeries {
    private String seriesID;
    private String teamA;
    private String teamB;
    private String format;
    private int startTime;
    private String server;

    public StartSeries() {

    }

    public StartSeries(String seriesID, String teamA, String teamB, String format, int startTime, String server) {
        this.seriesID = seriesID;
        this.teamA = teamA;
        this.teamB = teamB;
        this.format = format;
        this.startTime = startTime;
        this.server = server;
    }

    public String getSeriesID() {
        return seriesID;
    }

    public String getTeamA() {
        return teamA;
    }

    public String getTeamB() {
        return teamB;
    }

    public String getFormat() {
        return format;
    }

    public int getStartTime() {
        return startTime;
    }

    public String getServer() {
        return server;
    }
}
