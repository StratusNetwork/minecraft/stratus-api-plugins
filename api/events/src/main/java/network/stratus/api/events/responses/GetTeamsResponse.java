/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.events.responses;

import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.Team;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Represents a response to a check for team invites.
 * 
 * @author Ian Ballingall
 *
 */
public class GetTeamsResponse {

	private List<Team> teams;

	public List<Team> getTeams() {
		return teams;
	}

}
