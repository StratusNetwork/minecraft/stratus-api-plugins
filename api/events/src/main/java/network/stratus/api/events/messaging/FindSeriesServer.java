package network.stratus.api.events.messaging;

public class FindSeriesServer {
    private String seriesID;
    private String teamA;
    private String teamB;
    private String format;
    private int startTime;
    private String server;
    private String group;

    public FindSeriesServer() {

    }

    public FindSeriesServer(String seriesID, String teamA, String teamB, String format, int startTime, String server, String group) {
        this.seriesID = seriesID;
        this.teamA = teamA;
        this.teamB = teamB;
        this.format = format;
        this.startTime = startTime;
        this.server = server;
        this.group = group;
    }

    public String getSeriesID() {
        return seriesID;
    }

    public String getTeamA() {
        return teamA;
    }

    public String getTeamB() {
        return teamB;
    }

    public String getFormat() {
        return format;
    }

    public int getStartTime() {
        return startTime;
    }

    public String getServer() {
        return server;
    }

    public String getGroup() {
        return group;
    }
}
