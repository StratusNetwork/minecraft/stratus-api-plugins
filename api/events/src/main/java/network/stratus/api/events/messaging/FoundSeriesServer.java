package network.stratus.api.events.messaging;

public class FoundSeriesServer {
    private String seriesID;
    private String server;

    public FoundSeriesServer() {

    }

    public FoundSeriesServer(String seriesID, String server) {
        this.seriesID = seriesID;
        this.server = server;
    }

    public String getSeriesID() {
        return seriesID;
    }

    public String getServer() {
        return this.server;
    }
}
