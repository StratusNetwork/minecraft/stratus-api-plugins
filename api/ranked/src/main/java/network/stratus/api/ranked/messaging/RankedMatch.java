/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.messaging;
import java.util.List;

public class RankedMatch {

    private String rankedId;
    private String proxy;
    private String server;
    private String map;
    private String gamemode;
    private boolean premium;
    private boolean eloEnabled;
    private String seasonPostfix;
    private List<RankedPlayer> captain1;
    private List<RankedPlayer> captain2;
    private List<RankedPlayer> team1;
    private List<RankedPlayer> team2;
    private String queueId;
    private String channel1Id;
    private String channel2Id;
    private String pickString;

    private List<String> availableMaps;
    private String team1Veto;
    private String team2Veto;

    private String rankedDiscord;
    private String staffAlertsId;
    private String waitingRoomId;

    public RankedMatch() {

    }

    public RankedMatch(String rankedId, String proxy, String server, String map, String gamemode,
                       boolean premium, boolean eloEnabled, String seasonPostfix,
                       List<RankedPlayer> captain1, List<RankedPlayer> captain2,
                       List<RankedPlayer> team1, List<RankedPlayer> team2,
                       String queueId, String channel1Id, String channel2Id, String pickString,
                       List<String> availableMaps, String team1Veto, String team2Veto,
                       String rankedDiscord, String staffAlertsId, String waitingRoomId) {
        this.rankedId = rankedId;
        this.proxy = proxy;
        this.server = server;
        this.map = map;
        this.premium = premium;
        this.eloEnabled = eloEnabled;
        this.seasonPostfix = seasonPostfix;
        this.captain1 = captain1;
        this.captain2 = captain2;
        this.team1 = team1;
        this.team2 = team2;
        this.queueId = queueId;
        this.channel1Id = channel1Id;
        this.channel2Id = channel2Id;
        this.pickString = pickString;
        this.availableMaps = availableMaps;
        this.team1Veto = team1Veto;
        this.team2Veto = team2Veto;
        this.rankedDiscord = rankedDiscord;
        this.staffAlertsId = staffAlertsId;
        this.waitingRoomId = waitingRoomId;
    }

    public String getRankedId() {
        return rankedId;
    }

    public String getProxy() {
        return proxy;
    }

    public String getServer() {
        return server;
    }

    public String getMap() {
        return map;
    }

    public String getGamemode() {
        return gamemode;
    }

    public boolean isPremium() {
        return premium;
    }

    public boolean isEloEnabled() {
        return eloEnabled;
    }

    public String getSeasonPostfix() {
        return seasonPostfix;
    }

    public List<RankedPlayer> getTeam1() {
        return team1;
    }

    public List<RankedPlayer> getTeam2() {
        return team2;
    }

    public List<RankedPlayer> getCaptain1() {
        return captain1;
    }

    public List<RankedPlayer> getCaptain2() {
        return captain2;
    }

    public String getQueueId() {
        return queueId;
    }

    public String getChannel1Id() {
        return channel1Id;
    }

    public String getChannel2Id() {
        return channel2Id;
    }

    public String getRankedDiscord() {
        return rankedDiscord;
    }

    public String getStaffAlertsId() {
        return staffAlertsId;
    }

    public String getWaitingRoomId() {
        return waitingRoomId;
    }

    public String getPickString() {
        return pickString;
    }

    public List<String> getAvailableMaps() {
        return availableMaps;
    }

    public String getTeam1Veto() {
        return team1Veto;
    }

    public String getTeam2Veto() {
        return team2Veto;
    }
}
