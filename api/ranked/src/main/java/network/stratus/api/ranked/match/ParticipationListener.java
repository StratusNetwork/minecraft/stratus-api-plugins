/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.match;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import network.stratus.api.pgm.StratusAPIPGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchStartEvent;

/**
 * Listens to the match start event to record the participants at the start of
 * the {@link Match}.
 * 
 * @author Ian Ballingall
 *
 */
public class ParticipationListener implements Listener {

	private ParticipateInMatchManager manager;

	public ParticipationListener(ParticipateInMatchManager manager) {
		this.manager = manager;
	}

//	@EventHandler(priority = EventPriority.MONITOR)
//	public void onMatchStart(MatchStartEvent event) {
//		if (!StratusAPIPGM.get().getParticipantManager().isMatchRegistered(event.getMatch()))
//			return;
//
//		Match match = event.getMatch();
//		manager.registerParticipants(match.getParticipants());
//	}

}
