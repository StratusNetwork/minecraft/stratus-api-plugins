/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.permissions;

import java.util.List;
import java.util.UUID;

/**
 * Manages players' permissions on behalf of a plugin. This is done in a
 * {@link Group}-based way.
 * 
 * @author Ian Ballingall
 *
 */
public interface PermissionsManager {

	/**
	 * Obtain the {@link GroupsManager} associated with this
	 * {@code PermissionsManager}.
	 * 
	 * @return The {@link GroupsManager}
	 */
	public GroupsManager getGroupsManager();

	/**
	 * Sets the player's permissions given a list of nodes.
	 * 
	 * @param uuid        The {@link UUID} of the player whose permissions will be
	 *                    set
	 * @param permissions The {@link List} of permission nodes
	 */
	public void setPermissions(UUID uuid, List<String> permissions);

	/**
	 * Sets the player's permissions given a permissions {@link Group}.
	 * 
	 * @param uuid  The {@link UUID} of the player whose permissions will be set
	 * @param group The {@link Group} whose permissions will be added to the player
	 */
	public void setPermissions(UUID uuid, Group group);

	/**
	 * Sets a single permission on the given player.
	 * 
	 * @param uuid       The {@link UUID} of the player whose permissions will be
	 *                   set
	 * @param permission The permission node to set
	 */
	public void setPermission(UUID uuid, String permission);

	/**
	 * Set a single permission on the given player with the specified value.
	 * 
	 * @param uuid       The {@link UUID} of the player whose permissions will be
	 *                   set
	 * @param permission The permission node to set
	 * @param value      Whether to grant or negate this permission
	 */
	public void setPermission(UUID uuid, String permission, boolean value);

	/**
	 * Unset the given list of permissions from a player. This does not negate the
	 * permission nodes, but rather means this plugin will no longer manage this set
	 * of permission nodes.
	 * 
	 * @param uuid        The {@link UUID} of the player whose permissions will be
	 *                    unset
	 * @param permissions The list of permission nodes
	 */
	public void unsetPermissions(UUID uuid, List<String> permissions);

	/**
	 * Unset a single permission node from a player. This does not negate the
	 * permission node, but rather means this plugin will no longer manage this
	 * permission node.
	 * 
	 * @param uuid       The {@link UUID} of the player whose permissions will be
	 *                   unset
	 * @param permission The permission node to unset
	 */
	public void unsetPermission(UUID uuid, String permission);

	/**
	 * Detach the player player from the permissions manager. This means the plugin
	 * will stop managing this player's permissions. <b>This method must be called
	 * when the player disconnects.</b>
	 * 
	 * @param uuid The {@link UUID} of the player who will be detached
	 */
	public void detachPermissions(UUID uuid);

}
