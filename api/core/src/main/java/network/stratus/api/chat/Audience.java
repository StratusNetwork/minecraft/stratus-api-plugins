/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.chat;

import network.stratus.api.i18n.Translation;

/**
 * Represents an audience: a set of recipients for messages.
 * 
 * @author Ian Ballingall
 *
 */
public interface Audience {

	/**
	 * Translates the provided {@link Translation} into each audience member's
	 * locale, substitutes in any values, and sends it.
	 * 
	 * @param translation The {@link Translation} containing the translation key and
	 *                    values
	 */
	public void sendMessage(Translation translation);

	/**
	 * Translate the given string into each audience member's locale and send it.
	 * 
	 * @param stringKey The key for this string
	 */
	public void sendMessage(String stringKey);

	/**
	 * Translate the given string into each audience member's locale, substituting
	 * in the given arguments, and send it.
	 * 
	 * @param stringKey The key for this string
	 * @param args      The values to be substituted into the string
	 */
	public void sendMessage(String stringKey, Object... args);

	/**
	 * Sends the string directly to the audience without translation.
	 * 
	 * @param message The string to send
	 */
	public void sendMessageRaw(String message);

}
