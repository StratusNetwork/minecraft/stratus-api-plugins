/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.Team;

/**
 * Represents a request to obtain a {@link Team} from the backend.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamRequest implements Request<Team> {

	private String targetEndpoint;
	private String target;

	private TeamRequest(String targetEndpoint, String target) {
		this.targetEndpoint = targetEndpoint;
		this.target = target;
	}

	@Override
	public String getEndpoint() {
		return "/teams" + targetEndpoint + "/" + target;
	}

	@Override
	public Class<Team> getResponseType() {
		return Team.class;
	}

	@Override
	public Team make(APIClient client) {
		return client.get(this);
	}

	/**
	 * Create a request for a {@link Team} by its unique name.
	 * 
	 * @param teamName The team name
	 * @return The {@link Team} object
	 */
	public static TeamRequest teamName(String teamName) {
		return new TeamRequest("/name", teamName.replaceAll("/", "%2F"));
	}

	/**
	 * Create a request for a {@link Team} by its unique ID.
	 * 
	 * @param teamId The team ObjectId
	 * @return The {@link Team} object
	 */
	public static TeamRequest teamId(String teamId) {
		return new TeamRequest("/id", teamId);
	}

	/**
	 * Create a request to find a given player's {@link Team}.
	 * 
	 * @param playerName The player's username
	 * @return The {@link Team} object
	 */
	public static TeamRequest playerName(String playerName) {
		return new TeamRequest("/player/name", playerName.replaceAll("/", "%2F"));
	}

	/**
	 * Create a request to find a given player's {@link Team}.
	 * 
	 * @param uuid The player's {@link UUID}
	 * @return The {@link Team} object
	 */
	public static TeamRequest playerUuid(UUID uuid) {
		return new TeamRequest("/player/id", uuid.toString());
	}

}
