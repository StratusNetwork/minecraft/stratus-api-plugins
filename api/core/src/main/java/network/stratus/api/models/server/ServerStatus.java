/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models.server;

/**
 * Represents the online status of the server.
 * 
 * @author Ian Ballingall
 *
 */
public enum ServerStatus {
	ONLINE("ONLINE"), OFFLINE("OFFLINE");

	private String status;

	private ServerStatus(String status) {
		this.status = status;
	}

	/**
	 * Get the string representation of the online status.
	 * 
	 * @return The online status string
	 */
	public String getStatus() {
		return status;
	}
}
