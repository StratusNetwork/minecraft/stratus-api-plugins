/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

import java.util.UUID;

/**
 * A request to generate a registration code so a player can register on
 * the website or some other Stratus service.
 *
 * @author Ian Ballingall
 */
public class RegisterRequest implements Request<RegisterRequest.Response> {

	private final UUID uuid;
	private final String serverId;

	public RegisterRequest(UUID uuid, String serverId) {
		this.uuid = uuid;
		this.serverId = serverId;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getServerId() {
		return serverId;
	}

	@Override
	public String getEndpoint() {
		return "/players/register";
	}

	@Override
	public Class<RegisterRequest.Response> getResponseType() {
		return RegisterRequest.Response.class;
	}

	@Override
	public RegisterRequest.Response make(APIClient client) {
		return client.post(this);
	}

	/**
	 * The registration response containing the code.
	 */
	// This class is so trivial, there's no point keeping it separate.
	public static class Response {

		private String code;

		public String getCode() {
			return code;
		}

	}

}
