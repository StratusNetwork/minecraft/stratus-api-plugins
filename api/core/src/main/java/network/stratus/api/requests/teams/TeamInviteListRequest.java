/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.responses.teams.TeamInviteListResponse;

/**
 * A request to view pending invites for the sender's team.
 * 
 * @author Ian Ballingall
 *
 * @deprecated Invitation data to be retrieved from root team retrieval endpoints.
 *             Will be removed in API 7.
 */
@Deprecated
public class TeamInviteListRequest implements Request<TeamInviteListResponse> {

	private UUID sender;

	public TeamInviteListRequest(UUID sender) {
		this.sender = sender;
	}

	public UUID getSender() {
		return sender;
	}

	@Override
	public String getEndpoint() {
		return "/teams/invites/team/" + sender.toString();
	}

	@Override
	public Class<TeamInviteListResponse> getResponseType() {
		return TeamInviteListResponse.class;
	}

	@Override
	public TeamInviteListResponse make(APIClient client) {
		return client.get(this);
	}

}
