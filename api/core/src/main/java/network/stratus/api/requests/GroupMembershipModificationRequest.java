/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

/**
 * Represents a request to modify a user's group membership.
 * 
 * @author Ian Ballingall
 *
 */
public class GroupMembershipModificationRequest implements Request<Void> {

	/** The user effecting the modification. */
	private UUID issuer;
	/** The name of the user whose groups are being modified. */
	private String target;
	/** The name of the group they are being added to or removed from. */
	private String groupName;
	/** Whether this is an addition or removal. */
	@JsonIgnore
	private boolean add;

	public GroupMembershipModificationRequest(UUID issuer, String target, String groupName, boolean add) {
		this.issuer = issuer;
		this.target = target;
		this.groupName = groupName;
		this.add = add;
	}

	public UUID getIssuer() {
		return issuer;
	}

	@JsonIgnore
	public String getTarget() {
		return target;
	}

	@JsonIgnore
	public String getGroupName() {
		return groupName;
	}

	@JsonIgnore
	public boolean getAdd() {
		return add;
	}

	@Override
	public String getEndpoint() {
		return "/permissions/groups/membership/" + target + "/" + groupName;
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> map = new HashMap<>();
		if (issuer != null) {
			map.put("issuer", issuer);
		}

		return map;
	}

	@Override
	public Void make(APIClient client) {
		return add ? client.put(this) : client.delete(this);
	}

}
