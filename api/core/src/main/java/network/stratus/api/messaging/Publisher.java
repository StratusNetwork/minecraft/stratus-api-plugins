/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.messaging;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

/**
 * Encodes messages and transmits them over a messaging {@link Channel}.
 * Subclasses should handle serialisation and then publish the message.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class Publisher {

	private Channel channel;
	private String exchangeName;
	private String routingKey;
	private AMQP.BasicProperties properties;

	public Publisher(Channel channel, String exchangeName, String routingKey, AMQP.BasicProperties properties) {
		this.channel = channel;
		this.exchangeName = exchangeName;
		this.routingKey = routingKey;
		this.properties = properties;
	}

	public Channel getChannel() {
		return channel;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public String getRoutingKey() {
		return routingKey;
	}

	public AMQP.BasicProperties getProperties() {
		return properties;
	}

	/**
	 * Publish the provided message over this {@link Publisher}'s {@link Channel}, to its
	 * exchange, and with respect to its routing key and properties.
	 * 
	 * @param message The message as a POJO, which will be sent
	 * @throws IOException If an IO problem occurs when sending the message
	 */
	public abstract void publish(Object message) throws IOException;

}
