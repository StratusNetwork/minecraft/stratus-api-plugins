/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models;

import java.util.Date;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

/**
 * Represents a report made by one player against another. This model can be
 * sent and received directly from the backend, with no intermediate
 * representation required.
 * 
 * @author Ian Ballingall
 *
 */
public class Report implements Request<Report> {

	private String _id;
	private User reporter;
	private User target;
	private Date time;
	private String reason;
	private String serverName;

	/**
	 * Empty constructor to allow creation by Jackson.
	 */
	public Report() {
	}

	public Report(User reporter, User target, Date time, String reason, String serverName) {
		this.reporter = reporter;
		this.target = target;
		this.time = time;
		this.reason = reason;
		this.serverName = serverName;
	}

	public Report(String _id, User reporter, User target, Date time, String reason, String serverName) {
		this._id = _id;
		this.reporter = reporter;
		this.target = target;
		this.time = time;
		this.reason = reason;
		this.serverName = serverName;
	}

	public String get_id() {
		return _id;
	}

	public User getReporter() {
		return reporter;
	}

	public User getTarget() {
		return target;
	}

	public Date getTime() {
		return time;
	}

	public String getReason() {
		return reason;
	}

	public String getServerName() {
		return serverName;
	}

	@Override
	public String getEndpoint() {
		return "/reports";
	}

	@Override
	public Class<Report> getResponseType() {
		return Report.class;
	}

	@Override
	public Report make(APIClient client) {
		return client.post(this);
	}

}
