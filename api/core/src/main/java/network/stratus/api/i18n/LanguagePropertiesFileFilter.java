/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.i18n;

import java.io.File;
import java.io.FileFilter;

/**
 * Defines a filter for language properties files. This filter allows through it
 * only regular files matching the correct naming scheme. An example of a
 * correctly named file is <i>en_GB.properties</i>.
 * 
 * @author Ian Ballingall
 *
 */
public class LanguagePropertiesFileFilter implements FileFilter {

	@Override
	public boolean accept(File pathname) {
		return pathname.isFile() && pathname.getName().matches("[a-z]{2}_[A-Z]{2}\\.properties");
	}

}
