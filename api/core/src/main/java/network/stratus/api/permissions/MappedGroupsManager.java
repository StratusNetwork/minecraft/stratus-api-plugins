/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.permissions;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

/**
 * Manages group-based permissions by employing maps.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class MappedGroupsManager implements GroupsManager {

	/** Maps the group's database ID to the group object. */
	private Map<String, Group> groupsById;
	/** Maps the group's name to the group object. */
	private Map<String, Group> groupsByName;
	/** Lists the names of the default groups. */
	private List<String> defaultGroupNames;
	/** Maps a player's UUID to their group memberships. */
	private Map<UUID, SortedSet<Group>> playerGroups;

	public MappedGroupsManager(List<String> defaultGroupNames) {
		this.groupsById = new ConcurrentHashMap<>();
		this.groupsByName = new ConcurrentHashMap<>();
		this.defaultGroupNames = defaultGroupNames;
		this.playerGroups = new ConcurrentHashMap<>();
	}

	@Nullable
	@Override
	public Group getCachedGroupById(String id) {
		return groupsById.get(id);
	}

	@Nullable
	@Override
	public Group getCachedGroupByName(String name) {
		return groupsByName.get(name);
	}

	@Override
	public void addGroup(Group group) {
		groupsById.put(group.get_id(), group);
		groupsByName.put(group.getName(), group);
	}

	@Override
	public void addGroups(Collection<Group> groups) {
		for (Group g : groups) {
			addGroup(g);
		}
	}

	@Override
	public void removeGroupByName(String name) {
		Group remove = getCachedGroupByName(name);

		if (remove != null) {
			groupsByName.remove(name);
			groupsById.remove(remove.get_id());
		}
	}

	@Override
	public void removeGroupById(String id) {
		Group remove = getCachedGroupById(id);

		if (remove != null) {
			groupsById.remove(id);
			groupsByName.remove(remove.getName());
		}
	}

	@Override
	public void clearGroups() {
		groupsById.clear();
		groupsByName.clear();
	}

	@Override
	public SortedSet<Group> getGroupsFromList(Collection<String> groupIds) {
		SortedSet<Group> playerGroups = new TreeSet<Group>();
		for (String name : defaultGroupNames) {
			playerGroups.add(getStoredGroupByName(name));
		}

		for (String id : groupIds) {
			playerGroups.add(getStoredGroupById(id));
		}

		return playerGroups;
	}

	@Override
	public void addPlayerToGroup(UUID uuid, Group group) {
		SortedSet<Group> groups = playerGroups.get(uuid);
		if (groups == null) {
			groups = new TreeSet<>();
			playerGroups.put(uuid, groups);
		}

		groups.add(group);
	}

	@Override
	public void addPlayerToGroups(UUID uuid, SortedSet<Group> groups) {
		SortedSet<Group> currentGroups = playerGroups.get(uuid);
		if (currentGroups == null) {
			playerGroups.put(uuid, groups);
		} else {
			currentGroups.addAll(groups);
		}
	}

	@Override
	public SortedSet<Group> getPlayerGroups(UUID uuid) {
		return playerGroups.get(uuid);
	}

	@Override
	public boolean removePlayerFromGroup(UUID uuid, Group group) {
		SortedSet<Group> groups = playerGroups.get(uuid);
		return groups.remove(group);
	}

	@Override
	public void removePlayer(UUID uuid) {
		playerGroups.remove(uuid);
	}

}
