/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.responses.teams;

import java.util.List;
import network.stratus.api.models.User;

/**
 * A response to a request to list pending invites to a team.
 * 
 * @author Ian Ballingall
 *
 * @deprecated Invitation data to be retrieved from root team retrieval
 *             endpoints. Will be removed in API 7.
 */
@Deprecated
public class TeamInviteListResponse {

	private String _id;
	private String name;
	private List<User> invites;

	public String get_id() {
		return _id;
	}

	public String getName() {
		return name;
	}

	public List<User> getInvites() {
		return invites;
	}

}
