/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.client.RequestFailureException;

/**
 * Represents a single session for an online player. This model corresponds to a
 * backend-recorded session.
 *
 * @author Ian Ballingall
 */
@JsonIgnoreProperties(value = { "__v" })
public class Session implements Request<Session> {

	private String _id;
	private UUID playerUuid;
	private String ip;
	private String server;
	private Date startTime;
	private Date endTime;

	/**
	 * Empty constructor for Jackson.
	 */
	public Session() {
	}

	/**
	 * Construct a session for a newly joined player, starting at the current time.
	 *
	 * @param playerUuid The player's {@link UUID}
	 * @param ip         The player's connecting IP address
	 */
	public Session(UUID playerUuid, String ip) {
		this.playerUuid = playerUuid;
		this.ip = ip;
		this.startTime = new Date();
	}

	public Session(String _id, UUID playerUuid, String ip, Date startTime, Date endTime) {
		this._id = _id;
		this.playerUuid = playerUuid;
		this.ip = ip;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String get_id() {
		return _id;
	}

	public UUID getPlayerUuid() {
		return playerUuid;
	}

	public String getIp() {
		return ip;
	}

	public String getServer() {
		return server;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Override
	public String getEndpoint() {
		return "/players/sessions";
	}

	@Override
	public Class<Session> getResponseType() {
		return Session.class;
	}

	/**
	 * This will perform an 'upsert' for this session:<br>
	 * If {@code _id} is {@code null} or if the {@code _id} does not already exist,
	 * a new session will be created on the backend;<br>
	 * If {@code _id} is defined and corresponds to an existing session, this
	 * session will be updated.<br><br>
	 *
	 * <b>When manipulating the session belonging to an online player, use the
	 * safe methods {@link network.stratus.api.server.SessionManager#saveNewSession(Session, APIClient)}
	 * and {@link network.stratus.api.server.SessionManager#terminateSession(UUID, APIClient)} to avoid potential
	 * concurrency issues.</b>
	 */
	@Override
	public Session make(APIClient client) throws RequestFailureException {
		return client.post(this);
	}

}
