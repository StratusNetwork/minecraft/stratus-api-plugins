/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models;

/**
 * Represents a global broadcast being sent throughout the network.
 * 
 * @author Ian Ballingall
 *
 */
public class GlobalBroadcast {

	private String message;
	private String serverName;

	public GlobalBroadcast() {
	}

	public GlobalBroadcast(String message, String serverName) {
		this.message = message;
		this.serverName = serverName;
	}

	public String getMessage() {
		return message;
	}

	public String getServerName() {
		return serverName;
	}

}
